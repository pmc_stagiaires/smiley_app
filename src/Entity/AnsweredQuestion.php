<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnsweredQuestion
 *
 * @ORM\Table(name="answeredquestion", indexes={@ORM\Index(name="AnsweredQuestionQuestion", columns={"questionId"}), @ORM\Index(name="AnsweredQuestionSession", columns={"sessionId"}), @ORM\Index(name="AnsweredQuestionAnswer", columns={"answerId"})})
 * @ORM\Entity
 */
class AnsweredQuestion {
    /**
     * @var int
     *
     * @ORM\Column(name="answeredQuestionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $answeredQuestionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var int
     *
     * @ORM\Column(name="answerId", type="integer", length=100, nullable=false)
     * })
     */
    private $answerId;

    /**
     * @var \Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionId", referencedColumnName="questionId")
     * })
     */

    private $questionId;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sessionId", referencedColumnName="sessionId")
     * })
     */
    private $sessionId;

    /**
     * @return int
     */

    /**
     * @var string
     *
     * @ORM\Column(name="answeredAnswerValue", type="string", nullable=true)

     */
    private $answeredAnswerValue;

    public function getAnsweredQuestionId(): int
    {
        return $this->answeredQuestionId;
    }


    /**
     * @param int $answeredQuestionId
     * @return AnsweredQuestion
     */
    public function setAnsweredQuestionId(int $answeredQuestionId): AnsweredQuestion
    {
        $this->answeredQuestionId = $answeredQuestionId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return AnsweredQuestion
     */
    public function setCreatedDate(\DateTime $createdDate): AnsweredQuestion
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return AnsweredQuestion
     */
    public function setModifiedDate(\DateTime $modifiedDate): AnsweredQuestion
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return AnsweredQuestion
     */
    public function setDeletedDate(?\DateTime $deletedDate): AnsweredQuestion
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return AnsweredQuestion
     */
    public function setCreatedBy(string $createdBy): AnsweredQuestion
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return AnsweredQuestion
     */
    public function setModifiedBy(string $modifiedBy): AnsweredQuestion
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return AnsweredQuestion
     */
    public function setDeletedBy(?string $deletedBy): AnsweredQuestion
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     *
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * @param  $answerId
     * @return AnsweredQuestion
     */
    public function setAnswerId($answerId)
    {
        $this->answerId = $answerId;
        return $this;
    }

    /**
     *
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * @param  $questionId
     * @return AnsweredQuestion
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;
        return $this;
    }

    /**
     *
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param $sessionId
     * @return AnsweredQuestion
     */
    public function setSessionId($sessionId): AnsweredQuestion
    {
        $this->sessionId = $sessionId;
        return $this;
    }
    public function getAnsweredAnswerValue()
    {
        return $this->answeredAnswerValue;
    }
    public function setAnsweredAnswerValue($answeredAnswerValue)
    {
        $this->answeredAnswerValue = $answeredAnswerValue;
        return $this;
    }

    public function __toInt()
    {
        return $this->answerId;
    }

    public function __toInt2()
    {
        return $this->surveyId;
    }
}
