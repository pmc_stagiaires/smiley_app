<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190606114709 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE migrationversions CHANGE version version VARCHAR(14) NOT NULL');
        $this->addSql('ALTER TABLE user DROP salt, CHANGE role role VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE MigrationVersions CHANGE version version VARCHAR(14) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE User ADD salt VARCHAR(16) NOT NULL COLLATE utf8mb4_unicode_ci, CHANGE role role VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
