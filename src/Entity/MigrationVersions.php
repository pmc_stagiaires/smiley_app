<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MigrationVersions
 *
 * @ORM\Table(name="migration_versions")
 * @ORM\Entity
 */
class MigrationVersions {
    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=14, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $version;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="executedAt", type="date", nullable=false)
     */
    private $executedAt;

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return MigrationVersions
     */
    public function setVersion(string $version): MigrationVersions
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExecutedAt(): \DateTime
    {
        return $this->executedAt;
    }

    /**
     * @param \DateTime $executedAt
     * @return MigrationVersions
     */
    public function setExecutedAt(\DateTime $executedAt): MigrationVersions
    {
        $this->executedAt = $executedAt;
        return $this;
    }
}
