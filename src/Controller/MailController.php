<?php


namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\Mapping\Entity;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class MailController extends AbstractController {

    /**
     * Function to get the current user object
     */
    Public function getCurrentUser()
    {
        $currentUser = $this->getUser();

        return $currentUser;
    }

    /**
     * @Route("/send/registrationMail/{id}", name="sendRegistrationMail")
     * @param \Swift_Mailer $mailer
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendRegistrationMail($id, \Swift_Mailer $mailer, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $registrationUrl = $this->generateAbsoluteUrl($user, $request);

        try {
            $view = $this->renderView('emails/accountRegistrationMail.html.twig', array(
                'currentUser' => $user,
                'registrationUrl' => $registrationUrl
            ));
            $message = (new \Swift_Message('Account verificatie voor ' . $user->getEmail()))
                ->setFrom($this->getCurrentUser()->getEmail())
                ->setTo($user->getEmail())
                ->setBody($view, 'text/html'
                );

            $mailer->send($message);

            //to catch in case an error occurs
        } catch (Exception $exception) {
            echo $exception;
            return $this->redirectToRoute('manager');
        }

        return $this->redirectToRoute('manager');
    }

    /**
     * @Route("/send/accountResetMail/{id}", name="sendAccountResetMail")
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendAccountResetMail($id, \Swift_Mailer $mailer)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $message = (new \Swift_Message('Account reset'))
            ->setFrom($this->getCurrentUser()->getUsername())
            ->setTo($user->getEmail())
            ->setBody( "Account reset");

        $mailer->send($message);

        return $this->redirectToRoute('manager');
    }

    /**
     * @Route("/send/forgotPasswordMail/{id}", name="sendForgotPasswordMail")
     * @param \Swift_Mailer $mailer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function sendForgotPasswordMail($id, \Swift_Mailer $mailer)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        $message = (new \Swift_Message('Nieuw Wachtwoord'))
            ->setFrom($this->getCurrentUser()->getUsername())
            ->setTo($user->getEmail())
            ->setBody( "Nieuw Wachtwoord");

        $mailer->send($message);

        return $this->redirectToRoute('manager');
    }

    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($Entity);
        $entityManager->flush();
    }

    /**
     * This function generates an absolute url for the activation mail
     * @param $user
     * @return string
     */
    public function generateAbsoluteUrl($user, Request $request)
    {
        // Get the host (localhost works)
        $host = $request->getHost();

        // Generate the actual url
        $url = $host . $this->generateUrl('activate', array('activationCode' => $user->getActivationCode()));

        return $url;
    }
}