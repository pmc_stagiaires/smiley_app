<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Entity\Company;
use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Session;
use App\Entity\Survey;
use App\Entity\Page;
use App\Entity\Question;
use App\Entity\AnsweredPage;
use App\Entity\AnsweredQuestion;

use App\Entity\SurveyResult;
use App\Entity\User;
use App\Form\Survey\EditAnswerFormType;
use App\Form\Survey\EditQuestionFormType;
use App\Form\Survey\NewAnswerFormType;
use App\Form\Survey\newPageFormType;
use App\Form\Survey\NewQuestionFormType;
use App\Form\Survey\NewSurveyFormType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use datetime;

class SurveyController extends AbstractController {
    public function __construct(TokenStorageInterface $tokenStorage) {
        $this->tokenStorage = $tokenStorage;
    }

    Public function getCurrentUser() {
        $this->getUser();

        $user = null;
        $token = $this->tokenStorage->getToken();

        if ($token !== null) {
            $user = $token->getUsername();
        }

        return $user;
    }

    /**
     * @Route("surveys", name="surveys")
     *
     */
    public function showSurveys() {
        $currentUser = $this->getUser();
        $surveys = $this->getDoctrine()->getRepository(Survey::class)->findBy(array('companyId' => $currentUser->getCompanyId()));

        return $this->render('pages/survey.html.twig', [
            'surveys' => $surveys
        ]);
    }

    /**
     * @Route("/surveys/new", name="newSurvey")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function newSurvey (Request $request) {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $this->getCurrentUser()]);
        $company = $this->getDoctrine()->getRepository(Company::class)->findOneBy(['companyId' => $user->getCompanyId()]);

        $survey = new survey();

        $form = $this->createForm(NewSurveyFormType::class, $survey);

        $survey->setCreatedDate(new DateTime());
        $survey->setModifiedDate(new DateTime());
        $survey->setCreatedBy($this->getCurrentUser());
        $survey->setModifiedBy($this->getCurrentUser());
        $survey->setCompanyId($company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $survey = $form->getData();

            if (!empty($form->get('surveyBackgroundPath')->getData())) {

                $file = $form->get('surveyBackgroundPath')->getData();
                $filename = $this->generateUniqueFileName() . '.' . $file->guessExtension();

                try {
                    $file->move($this->getParameter('imagepath_directory'), $filename);
                } catch (FileException $e) {

                }
                $survey->setSurveyBackgroundPath($filename);
            }

            $this->saveToDatabase($survey);

            return $this->redirectToRoute('surveys');
        }

        return $this->render('pages/SurveyPages/newSurvey.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/surveys/{surveyId}", name="showSurvey")
     * @param $surveyId
     * @return Response
     */
    public function showSurvey($surveyId) {
        $Survey = $this->getDoctrine()->getRepository(survey::class)->find($surveyId);
        $Questions = $this->getDoctrine()->getRepository(Question::class)->findBy(['surveyId' => $surveyId]);
        $Pages = $this->getDoctrine()->getRepository(Page::class)->findBy(['surveyId' => $surveyId]);
        $Question = $this->getDoctrine()->getRepository(Question::class)->findBy(['surveyId' => $surveyId]);
        $Answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['questionId' => $Question]);

        return $this->render('pages/SurveyPages/showSurvey.html.twig', [
            'Survey' => $Survey,
            'Pages' => $Pages,
            'Questions' => $Questions,
            'Answers' => $Answers
        ]);
    }

    /**
     * @Route("/surveys/{surveyId}/Question/{questionId}", name="showQuestion")
     * @param $surveyId
     * @param $questionId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showQuestion($surveyId, $questionId) {
        $Survey = $this->getDoctrine()->getRepository(survey::class)->find($surveyId);
        $Question = $this->getDoctrine()->getRepository(Question::class)->find($questionId);
        $Answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(array('questionId' => $questionId));

        return $this->render('pages/SurveyPages/showQuestion.html.twig', [
            'Survey' => $Survey,
            'Question' => $Question,
            'Answers' => $Answers
        ]);
    }

    /**
     * @Route("/surveys/{surveyId}/newPage", name="newPage")
     * @Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @return RedirectResponse|Response
     */
    public function newPage(Request $request, $surveyId) {
        $Page = new Page();

        $form = $this->createForm(newPageFormType::class, $Page);

        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find(['surveyId' => $surveyId]);
        $Pages = $this->getDoctrine()->getRepository(Page::class)->findBy(['surveyId' => $surveyId]);

        $pagesCount = count($Pages);

        if ($pagesCount != 0) {
            $lastPage = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['pageNumber' => ($pagesCount), 'surveyId' => $surveyId]);
            $lastPage->setNextPage($pagesCount + 1);

            $this->saveToDatabase($lastPage);
        }

        $Page->setPageNumber($pagesCount + 1);
        $Page->setCreatedDate(new DateTime());
        $Page->setModifiedDate(new DateTime());
        $Page->setCreatedBy($this->getCurrentUser());
        $Page->setModifiedBy($this->getCurrentUser());
        $Page->setNextPage(0);
        $Page->setPreviousPage($pagesCount);
        $Page->setSurveyId($Survey);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $page = $form->getData();

            $this->addFlash('success', 'New page has been saved');
            $this->saveToDatabase($page);

            return $this->redirectToRoute('showSurvey', ['surveyId' => $surveyId]);
//              De return hierboven gaat naar de huidige survey pagina, mocht de return hieronder nodig zijn kun je deze evenuteel weghalen
//              return $this->render('pages/SurveyPages/newPage.html.twig', [
//                  'survey' => $Survey,
//                  'form' => $form->createView(), "Survey" => $Survey
//              ]);
            }

        return $this->render('pages/SurveyPages/newPage.html.twig', [
            'survey' => $Survey,
            'form' => $form->createView(), "Survey" => $Survey
        ]);
    }

    /**
     * @Route("/surveys/{surveyId}/{pageId}/newQuestion", name="newQuestion")
     * @Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @param $pageId
     * @return RedirectResponse|Response
     */
    public function newQuestion(Request $request, $surveyId, $pageId) {
        $Question = new Question();

        $form = $this->createForm(NewQuestionFormType::class, $Question);

        $survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $page = $this->getDoctrine()->getRepository(Page::class)->find($pageId);
        $questions = $this->getDoctrine()->getRepository(Question::class)->findBy(['surveyId' => $surveyId, 'pageId' => $pageId]);

        $questionsCount = count($questions);

        $Question->setQuestionNumber($questionsCount + 1);
        $Question->setCreatedDate(new DateTime());
        $Question->setModifiedDate(new DateTime());
        $Question->setCreatedBy($this->getCurrentUser());
        $Question->setModifiedBy($this->getCurrentUser());
        $Question->setPageId($page);
        $Question->setSurveyId($survey);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $Answer = $form->getData();

            $this->addFlash('success', 'New question has been saved');
            $this->saveToDatabase($Answer);

            return $this->redirectToRoute('showSurvey', ['surveyId' => $surveyId]);
            // De return hierboven gaat naar de huidige survey pagina, mocht de return hieronder nodig zijn kun je deze evenuteel weghalen

//            return $this->render('pages/SurveyPages/newQuestion.html.twig', [
//                'survey' => $survey,
//                'form' => $form->createView()
//            ]);
        }

        return $this->render('pages/SurveyPages/newQuestion.html.twig', [
            'survey' => $survey,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("surveys/{surveyId}/newAnswer/{questionId}", name="newAnswer")
     * @Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @param $questionId
     * @return RedirectResponse|Response
     */
    public function newAnswer(Request $request, $surveyId, $questionId) {
        $survey = $this->getDoctrine()->getRepository(Survey::class)->findOneBy(['surveyId' => $surveyId]);
        $Question = $this->getDoctrine()->getRepository(Question::class)->findOneBy(['questionId' => $questionId]);
        $nextQuestion = $this->getDoctrine()->getRepository(Question::class)->find($questionId);
        $questions = $this->getDoctrine()->getRepository(Question::class)->findBy(['surveyId' => $surveyId]);
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['surveyId' => $surveyId]);

        $answersCount = count($answers);

        $Answer = new Answer();

        $Answer->setAnswerNumber($answersCount + 1);
        $Answer->setCreatedDate(new DateTime());
        $Answer->setModifiedDate(new DateTime());
        $Answer->setCreatedBy($this->getCurrentUser());
        $Answer->setModifiedBy($this->getCurrentUser());
        $Answer->setSurveyId($survey);
        $Answer->setQuestionId($Question);
        $Answer->setNextQuestion($nextQuestion);


        $form = $this->createFormBuilder($Answer)
                        ->add('answerValue', TextType::class, [
                            'required' => true, 'attr' => [
                                'class' => 'form-control'
                            ]
                        ])
                        ->add('answerImagePath', FileType::class, [
                            'required' => false, 'attr' => [
                                'accept' => 'image/jpeg,image/png'
                            ]
                        ])
                        ->add('nextQuestion', EntityType::class, [
                            'label' => 'Next page',
                            'query_builder' => function(EntityRepository $repository) use ($surveyId) {
                                return $repository->createQueryBuilder('question')
                                    ->where('question.surveyId = :surveyId')
                                    ->setParameter('surveyId', $surveyId);
                            },
                            'required' => true, 'attr' => [
                                'data-id' => $surveyId,
                                'id' => 'sel1',
                                'class' => 'form-control'
                            ],
                            'class' => Question::class,
                            'choice_label' => function (Question $nextQuestion) {
                                return sprintf("Page: " .$nextQuestion->getPageId()->getPageNumber() . " Title:" . $nextQuestion->getQuestionTitle());
                            },
                        ])
                        ->add('save', SubmitType::class, [
                            'label' => 'Submit', 'attr' => [
                                'class' => 'btn btn-primary mt-3'
                            ]
                        ])
                        ->getForm();

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $Answer = $form->getData();

            $this->addFlash('success', 'New answer has been saved');

            if (!empty($form->get('answerImagePath')->getData())) {
                $file = $form->get('answerImagePath')->getData();
                $filename = $this->generateUniqueFileName().'.'.$file->guessExtension();

                try {
                    $file->move($this->getParameter('imagepath_directory'), $filename);
                } catch (FileException $e) {

                }
                $Answer->setAnswerImagePath($filename);
            }

            $this->saveToDatabase($Answer);


            return $this->render('pages/SurveyPages/newAnswer.html.twig', [
                'survey' => $survey,
                'Question' => $Question,
                'form' => $form->createView()
            ]);
        }

        return $this->render('pages/SurveyPages/newAnswer.html.twig', [
            'survey' => $survey,
            'Question' => $Question,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("surveys/edit/{surveyId}", name="editSurvey")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @return RedirectResponse|Response
     */
    public function editSurvey(Request $request, $surveyId) {
        $survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);

//        $form = $this->createForm(EditSurveyFormType::class, $survey);
        $defaultData = ['message' => 'Type your message here'];
        $form = $this->createFormBuilder($defaultData)
            ->add('surveyTitle', TextType::class, array('required' => false, 'attr' => array('class' => 'form-control')))
            ->add('surveyDescription', TextType::class, array('required' => false, 'attr' => array('class' => 'form-control')))
            ->add('surveyVersion', TextType::class, array('required' => false, 'attr' => array('class' => 'form-control')))
            ->add('surveyBackgroundPath', FileType::class,
                ['label' => 'Image background' , 'attr' => array('accept' => 'image/jpeg,image/png', 'data_class'
                => null, 'required' => false,  'allow_add'    => true )])
            ->add('save', SubmitType::class, array('label' => 'Update', 'attr' => array('class' => 'btn btn-primary
                mt-3')))->getForm();

        $survey->setModifiedDate(new DateTime());
        $survey->setModifiedBy($this->getCurrentUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');
            $file = $form->get('surveyBackgroundPath')->getData();
            $filename = $this->generateUniqueFileName() . '.' . $file->guessExtension();

            try {
                $file->move($this->getParameter('imagepath_directory'), $filename);
            } catch (FileException $e) {

            }

            $survey->setSurveyBackgroundPath($filename);

            $this->saveToDatabase(NULL);

            return $this->render('pages/SurveyPages/editSurvey.html.twig', [
                'form' => $form->createView()
            ]);
        }

        return $this->render('pages/SurveyPages/editSurvey.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("surveys/{surveyId}/editPage/{pageId}", name="editPage")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @param $pageId
     * @return RedirectResponse|Response
     */
    public function editPage(Request $request, $surveyId, $pageId) {
        $survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $Page = $this->getDoctrine()->getRepository(Page::class)->find($pageId);

        $form = $this->createFormBuilder($Page)
            ->add('pageNumber', IntegerType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('pageTitle', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('previousPage', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('nextPage', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Submit', 'attr' => [
                    'class' => 'btn btn-primary mt-3'
                ]])
            ->getForm();

        $Page->setModifiedDate(new DateTime());
        $Page->setModifiedBy($this->getCurrentUser());


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');

            $this->saveToDatabase(NULL);

            return $this->render('pages/SurveyPages/editPage.html.twig', [
                'form' => $form->createView(), "Page" => $Page
            ]);
        }
        return $this->render('pages/SurveyPages/editPage.html.twig', [
            'survey' => $survey,
            'form' => $form->createView(), "Page" => $Page
        ]);
    }

    /**
     * @Route("surveys/{surveyId}/Question/edit/{questionId}", name="editQuestion")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @param $questionId
     * @return RedirectResponse|Response
     */
    public function editQuestion(Request $request, $surveyId, $questionId) {
        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $Question = $this->getDoctrine()->getRepository(Question::class)->find($questionId);

        $form = $this->createForm(EditQuestionFormType::class, $Question);

        $Question->setModifiedDate(new DateTime());
        $Question->setModifiedBy($this->getCurrentUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');

            $this->saveToDatabase(NULL);

            return $this->render('pages/SurveyPages/editQuestion.html.twig', [
                'survey' => $Survey,
                'form' => $form->createView()
            ]);
        }
        return $this->render('pages/SurveyPages/editQuestion.html.twig', [
            'survey' => $Survey,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("surveys/{surveyId}/Answer/edit/{answerId}", name="editAnswer")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $surveyId
     * @param $answerId
     * @return RedirectResponse|Response
     */

    public function editAnswer(Request $request, $surveyId, $answerId) {
        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $Answer = $this->getDoctrine()->getRepository(Answer::class)->find($answerId);

        $form = $this->createFormBuilder($Answer)
            ->add('answerValue', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('answerImagePath', FileType::class, [
                'required' => false, 'attr' => [
                    'accept' => 'image/jpeg,image/png'
                ]
            ])
            ->add('nextQuestion', EntityType::class, [
                'label' => 'Next page',
                'query_builder' => function(EntityRepository $repository) use ($surveyId) {
                    return $repository->createQueryBuilder('question')
                        ->where('question.surveyId = :surveyId')
                        ->setParameter('surveyId', $surveyId);
                },
                'required' => true, 'attr' => [
                    'data-id' => $surveyId,
                    'id' => 'sel1',
                    'class' => 'form-control'
                ],
                'class' => Question::class,
                'choice_label' => function (Question $nextQuestion) {
                    return sprintf("Page: " .$nextQuestion->getPageId()->getPageNumber() . " Title:" . $nextQuestion->getQuestionTitle());
                },
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Submit', 'attr' => [
                    'class' => 'btn btn-primary mt-3'
                ]
            ])
            ->getForm();

        $Answer->setModifiedDate(new DateTime());
        $Answer->setModifiedBy($this->getCurrentUser());


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');

            $this->saveToDatabase(NULL);

            return $this->render('pages/SurveyPages/editAnswer.html.twig', [
                'survey' => $Survey,
                'form' => $form->createView()
            ]);
        }

        return $this->render('pages/SurveyPages/editAnswer.html.twig', [
            'survey' => $Survey,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/surveys/delete/{surveyId}")
     * @param $surveyId
     */
    public function deleteSurvey($surveyId) {
        $survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $this->addFlash('delete', 'Survey has been deleted!');

        if ($survey) {
            $this->removeFromDatabase($survey);
        }
    }

    /**
     * @Route("/surveys/page/delete/{pageId}")
     * @param $pageId
     */
    public function deletePage($pageId) {
        $page = $this->getDoctrine()->getRepository(Page::class)->find($pageId);
        $this->addFlash('delete', 'Page has been deleted!');

        if ($page) {
            $this->removeFromDatabase($page);
        }
    }

    /**
     * @Route("/surveys/question/delete/{questionId}")
     * @param $questionId
     */
    public function deleteQuestion($questionId) {
        $Question = $this->getDoctrine()->getRepository(Question::class)->find($questionId);
        $this->addFlash('delete', 'Question has been deleted!');

        if ($Question) {
            $this->removeFromDatabase($Question);
        }
    }

    /**
     * @Route("/surveys/answer/delete/{answerId}")
     * @param $answerId
     */
    public function deleteAnswer($answerId) {
        $answer = $this->getDoctrine()->getRepository(Answer::class)->find($answerId);
        $this->addFlash('delete', 'Survey has been deleted!');

        if ($answer) {
            $this->removeFromDatabase($answer);
        }
    }

    /**
     * @Route("surveys/display/{surveyId}/{sessionId}/submit/{surveyAction}", name="submitSurvey")
     * @Method("POST")
     * @param $surveyId
     * @param $sessionId
     * @param $surveyAction
     * @return RedirectResponse
     */
    public function submitSurvey($surveyId, $sessionId, $surveyAction) {
        $arr = $_POST;

        $currentUser = $this->getCurrentUser();
        $currentDateTime = new DateTime();

        $arrAnsweredPageIds = [];

        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $Session = $this->getDoctrine()->getRepository(Session::class)->find($sessionId);
        $Device = $this->getDoctrine()->getRepository(Device::class)->find($Session->getdeviceId());

        foreach ($arr as $answerName => $answerValue) {
            if ($answerValue != 0 || !empty($answerValue)) {
                $data = explode("form", $answerName);
                $pageId = $data[0];
                $questionId = $data[1];
                $answerId = $data[2];
                $answerIds = 1;

                $Question = $this->getDoctrine()->getRepository(Question::class)->find($questionId);
                $Answer = $this->getDoctrine()->getRepository(Answer::class)->find($answerId);
                $Page = $this->getDoctrine()->getRepository(Page::class)->find($pageId);

                array_push($arrAnsweredPageIds, $pageId);

                $answeredQuestion = new AnsweredQuestion();
                $answeredQuestion->setQuestionId($Question)
                    ->setAnswerId($Answer)
                    ->setAnsweredAnswerValue($answerValue)
                    ->setCreatedBy($currentUser)
                    ->setCreatedDate($currentDateTime)
                    ->setModifiedBy($currentUser)
                    ->setModifiedDate($currentDateTime)
                    ->setSessionId($Session)
                    ->setAnswerId($answerId);

                $answeredPage = new AnsweredPage();
                $answeredPage->setPageId($Page)
                    ->setSurveyId($Survey)
                    ->setAnsweredQuestionId($answeredQuestion)
                    ->setCreatedBy($currentUser)
                    ->setCreatedDate($currentDateTime)
                    ->setModifiedBy($currentUser)
                    ->setModifiedDate($currentDateTime)
                    ->setSessionId($Session);

                $this->saveToDatabase($answeredQuestion);
                $this->saveToDatabase($answeredPage);
            }
        }

        $strAnsweredPageIds = implode(", ",$arrAnsweredPageIds);

        $surveyResult = new SurveyResult();
        $surveyResult->setCreatedBy($currentUser)
            ->setCreatedDate($currentDateTime)
            ->setModifiedBy($currentUser)
            ->setModifiedDate($currentDateTime)
            ->setAnsweredPageIds($strAnsweredPageIds)
            ->setDeviceId($Device)
            ->setLocationId($Device->getLocationId())
            ->setSessionId($Session)
            ->setSurveyId($Survey);


        $this->saveToDatabase($surveyResult);

        if ($surveyAction == 'end') {
            return $this->redirectToRoute('surveys');
        } else if ($surveyAction == 'restart') {
            return $this->redirectToRoute('displaySurvey', ['surveyId' => $surveyId]);
        }

        return $this->redirectToRoute('surveys');
    }

    /**
     * @Route("surveys/display/{surveyId}", name="displaySurvey")
     * @param $surveyId
     * @return Response
     */
    public function displaySurvey($surveyId) {
        //TODO: device ID wordt nog niet van het apparaat afgelezen en is voor nu statisch omdat de enqête niet op een device wordt afgenomen.
        //deviceId is for testing purposes, this will be automatic soon
        $deviceId = 1;

        $surveyID = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyID);
        $Page = $this->getDoctrine()->getRepository(Page::class)->findBy(['surveyId' => $surveyID]);
        $Question = $this->getDoctrine()->getRepository(Question::class)->findBy(['surveyId' => $surveyID]);
        $Answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['questionId' => $Question]);
        $Device = $this->getDoctrine()->getRepository(Device::class)->find($deviceId);
        $answeredQuestions = $this->getDoctrine()->getRepository(AnsweredQuestion::class)->findAll();
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findAll();

        $Session = new Session();
        $Session->setCreatedBy($this->getCurrentUser())
            ->setCreatedDate(new DateTime())
            ->setModifiedBy($this->getCurrentUser())
            ->setModifiedDate(new DateTime())
            ->setDeviceId($Device)
            ->setSurveyId($Survey);

        $this->saveToDatabase($Session);

        $form = $this->createFormBuilder($Answers)
            ->add('answerValue', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->getForm();

        return $this->render('pages/SurveyPages/displaySurvey.html.twig', [
            'Survey' => $Survey,
            'Page' => $Page,
            'answeredQuestions' => $answeredQuestions,
            'Question' => $Question,
            'answers' => $answers,
            'Answers' => $Answers,
            'Session' => $Session,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("surveys/results/{surveyId}", name="displayResults")
     * @param $surveyId
     * @return Response
     */
    public function displaySurveyResults($surveyId) {
        $surveyResults = $this->getDoctrine()->getRepository(SurveyResult::class)->findBy(['surveyId' => $surveyId]);
        $pages = $this->getDoctrine()->getRepository(Page::class)->findBy(['surveyId' => $surveyId]);

        return $this->render('pages/SurveyPages/displaySurveyResults.html.twig', [
            'surveyResults' => $surveyResults,
            'pages' => $pages
        ]);
    }

    /**
     * @Route("surveys/results/{surveyId}/page/{pageId}", name="displayPageResults")
     * @param $surveyId
     * @param $pageId
     * @return Response
     */
    public function displayPageResults($surveyId, $pageId) {
        $survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);
        $answeredQuestions = $this->getDoctrine()->getRepository(AnsweredQuestion::class)->findAll();
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findAll();
        $locations = $this->getDoctrine()->getRepository(Location::class)->findOneBy(['companyId' => $survey->getCompanyId()]);
        $questions = $this->getDoctrine()->getRepository(Question::class)->findBy(['pageId' => $pageId]);
        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['pageId' => $pageId]);


        return $this->render('pages/SurveyPages/displayPageResults.html.twig', [
            'survey' => $survey,
            'answeredQuestions' => $answeredQuestions,
            'answers' => $answers,
            'locations' => $locations,
            'questions' => $questions,
            'page' => $page
        ]);
    }

    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity) {
        $entityManager = $this->getDoctrine()->getManager();

        if (!empty($Entity)) {
            $entityManager->persist($Entity);
        }

        $entityManager->flush();
    }

    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    public function removeFromDatabase($Entity) {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($Entity);
        $entityManager->remove($Entity);

        $entityManager->flush();
    }

    /**
     * @Route("surveys/NPS/{surveyId}", name="NetPromoterScore")
     * @param $surveyId
     */
    public function calNetPromoterScore($surveyId){
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(["surveyId" => $surveyId]);
        $answeredQuestion = [];
        foreach ($answers as $answer){
            if($answer->getAnswerType() == 'NetPromoterScore'){
                array_push($answeredQuestion, $answer->getAnswerId());
            }
        }

        $negative = 0;
        $positive = 5.0;
        $middle = 3.0;
        $answeredAnswerValue = $this->getDoctrine()->getRepository(AnsweredQuestion::class)->findBy(["answerId" => $answeredQuestion]);

        for ($i = 0; $i < count($answeredAnswerValue); $i++) {
            $value = $answeredAnswerValue[$i]->getAnsweredAnswerValue();
            if ($value > 0 && $value <= 6) {
                $negative++;
            } else if ($value >= 7 && $value <= 8) {
                $middle++;
            } else if ($value >= 9 && $value <= 10) {
                $positive++;
            }
        }

        $total = $negative + $positive + $middle;

        if ($total != 0) {
            if ($positive == 0) {
                $positivePer = 0;
            } else {
                $positivePer = 100 / $total * $positive;
            }

            if ($negative == 0) {
                $negativePer = 0;
            } else {
                $negativePer = 100 / $total * $negative;
            }
            $net = $positivePer - $negativePer;
        } else {
            $net = 0;
        }

        echo 'The netpromoter score = '.round($net, 2);
        exit();
    }
}