<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Survey
 *
 * @ORM\Table(name="survey", indexes={@ORM\Index(name="SurveyPage", columns={"pageId"}), @ORM\Index(name="SurveyResult", columns={"surveyResultsId"}), @ORM\Index(name="SurveyLocation", columns={"locationId"})})
 * @ORM\Entity
 */
class Survey {
    /**
     * @var int
     *
     * @ORM\Column(name="surveyId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $surveyId;

    /**
     * @var string
     *
     * @ORM\Column(name="surveyTitle", type="string", length=100, nullable=false)
     */
    private $surveyTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="surveyDescription", type="string", length=250, nullable=false)
     */
    private $surveyDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="surveyVersion", type="string", length=25, nullable=false)
     */
    private $surveyVersion;

//    /**
//     * @var int|null
//     *
//     * @ORM\Column(name="pageId", type="string", length=100, nullable=true)
//     */
//    private $pageId;
//
//    /**
//     * @var int|null
//     *
//     * @ORM\Column(name="surveyResultsId", type="string", length=100, nullable=true)
//     */
//    private $surveyResultsId;
//
//    /**
//     * @var int|null
//     *
//     * @ORM\Column(name="locationId", type="string", length=100, nullable=true)
//     */
//    private $locationId;

    /**
     * @var string|null
     *
     *
     * @ORM\Column(name="surveyBackgroundPath", type="string", length=50, nullable=true)
     */
    private $surveyBackgroundPath;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companyId", referencedColumnName="companyId")
     * })
     */
    private $companyId;

    /**
     * @return int
     */
    public function getSurveyId(): int
    {
        return $this->surveyId;
    }

    /**
     * @param int $surveyId
     * @return Survey
     */
    public function setSurveyId(int $surveyId): Survey
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurveyTitle()
    {
        return $this->surveyTitle;
    }

    /**
     * @param string $surveyTitle
     * @return Survey
     */
    public function setSurveyTitle(string $surveyTitle): Survey
    {
        $this->surveyTitle = $surveyTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurveyDescription()
    {
        return $this->surveyDescription;
    }

    /**
     * @param string $surveyDescription
     * @return Survey
     */
    public function setSurveyDescription(string $surveyDescription): Survey
    {
        $this->surveyDescription = $surveyDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurveyVersion()
    {
        return $this->surveyVersion;
    }

    /**
     * @param string $surveyVersion
     * @return Survey
     */
    public function setSurveyVersion(string $surveyVersion): Survey
    {
        $this->surveyVersion = $surveyVersion;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSurveyBackgroundPath()
    {
        return $this->surveyBackgroundPath;
    }

    /**
     * @param null|string $surveyBackgroundPath
     * @return Survey
     */
    public function setSurveyBackgroundPath(string $surveyBackgroundPath)
    {
        $this->surveyBackgroundPath = $surveyBackgroundPath;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Survey
     */
    public function setCreatedDate(\DateTime $createdDate): Survey
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Survey
     */
    public function setModifiedDate(\DateTime $modifiedDate): Survey
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Survey
     */
    public function setDeletedDate(?\DateTime $deletedDate): Survey
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Survey
     */
    public function setCreatedBy(string $createdBy): Survey
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Survey
     */
    public function setModifiedBy(string $modifiedBy): Survey
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Survey
     */
    public function setDeletedBy(?string $deletedBy): Survey
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return \Company
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param \Company $companyId
     * @return Survey
     */
    public function setCompanyId($companyId): Survey
    {
        $this->companyId = $companyId;
        return $this;
    }

    public function __ToInt()
    {
        return $this->surveyId;
    }
}
