<?php


namespace App\Controller;

use App\Entity\Answer;
use App\Entity\Company;
use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Question;
use App\Entity\Session;
use App\Entity\Survey;
use App\Entity\User;
use App\Form\Device\EditDeviceFormType;
use App\Form\Device\NewDeviceFormType;
use App\Repository\DeviceRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Tests\Fixtures\AnnotationFixtures\LocalizedPrefixLocalizedActionController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use DateTime;
use Symfony\Component\Validator\Validation;
use App\Helper\ResponseBuilder;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use function MongoDB\BSON\toJSON;

/**
 * @property TokenStorageInterface tokenStorage
 */
class DeviceController extends AbstractController
{
    /**
     * @Route("devices", name="devicePage")
     * @Method({"GET"})
     */
    public function index()
    {
        $currentUser = $this->getUser();
        $currentRole = $currentUser->getRole();
        if ($currentRole == 'ROLE_SUPER_ADMIN') {
            $device = $this->getDoctrine()->getRepository(Device::class)->findAll();
            $locations = $this->getDoctrine()->getRepository(Location::class)->findAll();
        } else {
            $locations = $this->getDoctrine()->getRepository(Location::class)->findBy(array('companyId' => $currentUser->getCompanyId()));
            $device = $this->getDoctrine()->getRepository(Device::class)->findBy(['locationId' => $locations]);
        }


        return $this->render('pages/device.html.twig', [
            'devices' => $device,
            'locations' => $locations
        ]);
    }

    public function getCurrentUser()
    {
        $currentUser = $this->getUser();

        if ($currentUser) {
            return $currentUser;
        } else {
            return $this->redirectToRoute('login');
        }
    }

//    /**
//     * List all devices
//     * @Rest\Get("/devices/test")
//     *
//     * @return Response
//     */
//    public function getDevice()
//    {
//        $repository = $this->getDoctrine()->getRepository(Device::class);
//        $devices = $repository->findAll();
//        return new JsonResponse($devices);
//    }


    /**
     * @Route(path="/devices/deroll", name="hotel_deroll")
     * @param Request $request
     * @return Response
     */
    public function derollDevice(Request $request, $deviceId)
    {
        $requestData = json_decode($request->getContent());
        $company = $this->getDoctrine()->getRepository(Company::class)->findAll();
        $devices = $this->getDoctrine()->getRepository(Device::class)->findAll();
        if ($devices == null) {
            return new Response(ResponseBuilder::build([], 404, "Hotel not found"), 404);
        }

        if (count($devices) == 0) {
            return new Response(ResponseBuilder::build([], 404, "Enrollment not found"), 404);
        }
        foreach ($devices as $userHotel) {
            $this->getDoctrine()->getManager()->remove($userHotel);
            $devices->delete($deviceId);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response(ResponseBuilder::build([]));
    }
    /*
         *
         *
         *
         * VERDER GAAN BIJ
         * https://www.adcisolutions.com/knowledge/getting-started-rest-api-symfony-4
         * EN DAN DE cmd+f = "publicfunctionpostMovieAction"
         *
         *
    */


    /**
     * @Route("/devices/new", name="newDevices")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param DeviceRepository $deviceRepository
     * @param $deviceId
     * @return RedirectResponse|Response
     * @throws \Exception
     */

    // This function is to connect the device to the backend
    public function new(Request $request, DeviceRepository $deviceRepository,$deviceId = null)
    {
        $device = new device();

//        $currentUser = $this->getUser();
//        $currentRole = $currentUser->getRole();
//        if ($currentRole == "ROLE_SUPER_ADMIN") {
//            $device = $this->getDoctrine()->getRepository(Device::class)->findAll();
//        } else {
//            $locations = $this->getDoctrine()->getRepository(Location::class)->findBy(array('companyId' => $currentUser->getCompanyId()));
//            $device = $this->getDoctrine()->getRepository(Device::class)->findBy(['locationId' => $locations]);
//        }

        $deviceLicense = $device->getDeviceLicense();


        if (!$deviceLicense) {

            $device->setCreatedDate(new \DateTime());
            $device->setModifiedDate(new \DateTime());
            $device->setCreatedBy($this->getCurrentUser()->getuserId());
            $device->setModifiedBy($this->getCurrentUser()->getuserId());
            $deviceLicense = $device->getDeviceLicense();

            $form = $this->createFormBuilder($device)
                ->add('deviceLicense', TextType::class,
                    array('attr' => array('class' => 'form-control', 'autocomplete' => 'off')))
                ->add('save', SubmitType::class, array('label' => 'Sumbit', 'attr' => array('class' => 'btn btn-primary
                mt-3')))
                ->getForm();


            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {
                $deviceCheck = $deviceRepository->findOneBy([
                    'deviceLicense' => $form->get('deviceLicense')->getData()
                ]);


                if(is_null($deviceCheck)) {

                    $this->addFlash('error', 'No device found');

                } else {
                    $this->addFlash('success', 'New device has been added');

                    return $this->redirectToRoute('connect_device', array('deviceId' => $deviceCheck->getDeviceId())); // cast $deviceId to string
                }


            }

            return $this->render('pages/DevicePages/newDevice.html.twig', [
                'form' => $form->createView()
            ]);
        }
    }

    /**
     * @Route("/devices/edit/{deviceId}", name="edit_device")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $deviceId
     * @param $surveyId
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, $deviceId)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($deviceId);
//        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);

        $form = $this->createFormBuilder($device)
            ->add('deviceLicense', TextType::class,
                array('attr' => array('class' => 'form-control')))
            ->add('locationId', EntityType::class, ['class' => Location::class,
                'choice_label' => function (Location $device) {
                    return sprintf($device->getAddress(), $device->getAddress());
                },
                'placeholder' => 'Choose an location',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('surveyId', EntityType::class, ['class' => Survey::class,
                'choice_label' => function (Survey $device) {
                    return sprintf('Id: ' . $device->getSurveyId() . ' Title: ' . $device->getSurveyTitle());
                },
                'placeholder' => 'Choose an survey',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'Set device active ',
                'required' => false,
                'attr' => ['class' => 'checkbox']
            ])
            ->add('save', SubmitType::class, array('label' => 'Update', 'attr' => array('class' => 'btn btn-primary
                mt-3')))
            ->getForm();

        $form->handleRequest($request);

        $device->setModifiedDate(new \DateTime());
        $device->setModifiedBy($this->getCurrentUser()->getuserId());

//        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');

            $this->saveToDatabase($device);

            return $this->redirect('devicePage');
        }

        return $this->render('pages/DevicePages/editDevice.html.twig', [
            'form' => $form->createView()
        ]);

    }


    /**
     * @Route("/devices/connect/{deviceId}", name="connect_device")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $deviceId
     * @param $surveyId
     * @return RedirectResponse|Response
     */
    public function connect(Request $request, $deviceId)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($deviceId);
//        $Survey = $this->getDoctrine()->getRepository(Survey::class)->find($surveyId);

        $response = json_decode($request->getContent());

        $em = $this->getDoctrine()->getManager();

        $device_already = $em->getRepository('App:Device')->find($deviceId);

        if (!$device_already) {
            throw $this->createNotFoundException('No device found for id '.$deviceId);
        }

        $deviceLicense = $device_already->getDeviceLicense();

        if ($deviceLicense) {
//            throw $this->createNotFoundException('No device found for id '.$deviceLicense);
//            echo 'devicelicense is'. $deviceLicense;
        }


        $form = $this->createFormBuilder($device)
            ->add('deviceLicense', TextType::class,
                array('attr' => array('class' => 'form-control')))
            ->add('locationId', EntityType::class, ['class' => Location::class,
                'choice_label' => function (Location $device) {
                    return sprintf($device->getAddress(), $device->getAddress());
                },
                'placeholder' => 'Choose an location',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('surveyId', EntityType::class, ['class' => Survey::class,
                'choice_label' => function (Survey $device) {
                    return sprintf('Id: ' . $device->getSurveyId() . ' Title: ' . $device->getSurveyTitle());
                },
                'placeholder' => 'Choose an survey',
                'required' => false,
                'attr' => ['class' => 'form-control']
            ])
            ->add('isActive', CheckboxType::class, [
                'label' => 'Set device active ',
                'required' => false,
                'attr' => ['class' => 'checkbox']
            ])
            ->add('save', SubmitType::class, array('label' => 'Update', 'attr' => array('class' => 'btn btn-primary
                mt-3')))
            ->getForm();

        $form->handleRequest($request);

//        $form = $this->createForm(EditDeviceFormType::class, $device);

        $device->setModifiedDate(new \DateTime());
        $device->setModifiedBy($this->getCurrentUser()->getuserId());
        $device->setCreatedBy($this->getCurrentUser()->getuserId());

//        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');

            $this->saveToDatabase($device);

            return $this->redirectToRoute('devicePage');
        }

        return $this->render('pages/DevicePages/connectDevice.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/devices/{deviceId}", name="device_show")
     * @param $deviceId
     * @return Response
     */
    public function show($deviceId)
    {
        $device = $this->getDoctrine()->getRepository(device::class)->find($deviceId);

        return $this->render('pages/DevicePages/showDevice.html.twig', array('device' => $device));
    }

//    /**
//     * @Route("/devices/delete/{deviceId}")
//     * @param $deviceId
//     */
//    public function delete($deviceId)
//    {
//        $this->addFlash('delete', 'Device has been deleted!');
//        $device = $this->getDoctrine()->getRepository(device::class)->find($deviceId);
//
//        $this->removeFromDatabase($device);
//
//        $response = new Response();
//        $response->send();
//
//        return $this->redirectToRoute('devicePage');
//
//    }


    /**
     * @Route("/devices/delete/{deviceId}-{deactivated}")
     * @Method({"GET"})
     * @param $deviceId
     * @param $deactivated
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivate($deviceId, $deactivated)
    {
        $device = $this->getDoctrine()->getRepository(Device::class)->find($deviceId);

        if ($device->getDeviceId() != $this->getCurrentUser()->getEmail()) {
            if ($deactivated == 1) {
                $device->setDeactivated(NULL);
                $device->setDeletedBy(NULL);
                $device->setDeletedDate(NULL);
            } else {
                $device->setDeactivated(1);
                $device->setDeletedBy($this->getCurrentUser()->getuserId());
                $device->setDeletedDate(new DateTime());
            }

            $this->saveToDatabase($device);
            $this->addFlash('delete', 'Device has been deleted');

            $response = new Response();
            $response->send();

            return $this->redirectToRoute('devicePage');

        } else {
            $this->addFlash('error', 'You don\'t currently have permission to deactivate this user.');
            return $this->redirectToRoute('devicePage');
        }
    }

    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if (!empty($Entity)) {
            $entityManager->persist($Entity);
        }

        $entityManager->flush();
    }

    public function removeFromDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($Entity);
        $entityManager->remove($Entity);

        $entityManager->flush();
    }

}