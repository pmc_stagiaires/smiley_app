<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Device
 *
 * @ORM\Table(name="device", indexes={@ORM\Index(name="DeviceLocation", columns={"locationId"})})
 * @ORM\Entity
 * @UniqueEntity(fields={"uniqueDeviceId"}, message="There is already an device with this id")
 */
class Device {
    /**
     * @var int
     *
     * @ORM\Column(name="deviceId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="deviceLicense", type="string", length=25, nullable=false)
     */
    private $deviceLicense;

    /**
     * @var string
     *
     * @ORM\Column(name="uniqueDeviceId", type="string", length=25, nullable=false)
     */
    private $uniqueDeviceId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="deactivated", type="boolean", nullable=true)
     */
    private $deactivated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @var \Location
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="locationId", referencedColumnName="locationId")
     * })
     */
    private $locationId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean", length=1, nullable=true)
     */
    private $isActive;

    /**
     * @return int
     */
    public function getDeviceId(): int
    {
        return $this->deviceId;
    }

    /**
     * @param int $deviceId
     * @return Device
     */
    public function setDeviceId(int $deviceId)
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceLicense()
    {
        return $this->deviceLicense;
    }


    /**
     * @param string $deviceLicense
     * @return Device
     */
    public function setDeviceLicense(string $deviceLicense): Device
    {
        $this->deviceLicense = $deviceLicense;
        return $this;
    }

    /**
     * @return string
     */
    public function getUniqueDeviceId()
    {
        return $this->uniqueDeviceId;
    }

    /**
     * @param string $uniqueDeviceId
     * @return Device
     */
    public function setUniqueDeviceId(string $uniqueDeviceId): Device
    {
        $this->uniqueDeviceId = $uniqueDeviceId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Device
     */
    public function setCreatedDate(\DateTime $createdDate): Device
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Device
     */
    public function setDeletedDate(?\DateTime $deletedDate): Device
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Device
     */
    public function setModifiedDate(\DateTime $modifiedDate): Device
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Device
     */
    public function setCreatedBy(string $createdBy): Device
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Device
     */
    public function setModifiedBy(string $modifiedBy): Device
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Device
     */
    public function setDeletedBy(?string $deletedBy): Device
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDeactivated()
    {
        return $this->deactivated;
    }

    /**
     * @param bool|null $deactivated
     * @return device
     */
    public function setDeactivated(?bool $deactivated)
    {
        $this->deactivated = $deactivated;
        return $this;
    }


    /**
     * @return \Survey
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param \Survey $surveyid
     * @return Device
     */
    public function setSurveyid($surveyId)
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    /**
     * @return \Location
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param \Location $locationId
     * @return Device
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param $isActive
     * @return Device
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    public function __ToInt()
    {
        return $this->deviceId;
    }
}
