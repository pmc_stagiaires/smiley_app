<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session", indexes={@ORM\Index(name="SessionDevice", columns={"deviceId"})})
 * @ORM\Entity
 */
class Session {
    /**
     * @var int
     *
     * @ORM\Column(name="sessionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sessionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var \Device
     *
     * @ORM\ManyToOne(targetEntity="Device")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deviceId", referencedColumnName="deviceId")
     * })
     */
    private $deviceId;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @return int
     */
    public function getSessionId(): int
    {
        return $this->sessionId;
    }

    /**
     * @param int $sessionId
     * @return Session
     */
    public function setSessionId(int $sessionId): Session
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Session
     */
    public function setCreatedDate(\DateTime $createdDate): Session
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Session
     */
    public function setModifiedDate(\DateTime $modifiedDate): Session
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Session
     */
    public function setDeletedDate(?\DateTime $deletedDate): Session
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Session
     */
    public function setCreatedBy(string $createdBy): Session
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Session
     */
    public function setModifiedBy(string $modifiedBy): Session
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Session
     */
    public function setDeletedBy(?string $deletedBy): Session
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return \Device
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param Device $deviceId
     * @return Session
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    public function getSurveyId()
    {
        return $this->surveyId;
    }

    public function setSurveyId($surveyId): Session
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    public function __ToInt()
    {
        return $this->sessionId;
    }
}
