<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location", indexes={@ORM\Index(name="CompanyLocation", columns={"companyId"})})
 * @ORM\Entity
 */
class Location {
    /**
     * @var int
     *
     * @ORM\Column(name="locationId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $locationId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=10, nullable=false)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=false)
     */
    private $city;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="deactivated", type="boolean", nullable=true)
     */
    private $deactivated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="subLocation", type="string", length=100, nullable=true)
     */
    private $subLocation;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companyId", referencedColumnName="companyId")
     * })
     */
    private $companyId;

    /**
     * @return int
     */
    public function getLocationId(): int
    {
        return $this->locationId;
    }

    /**
     * @param int $locationId
     * @return Location
     */
    public function setLocationId(int $locationId): Location
    {
        $this->locationId = $locationId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Location
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return Location
     */
    public function setPostalCode(string $postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Location
     */
    public function setCity(string $city): Location
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Location
     */
    public function setCreatedDate(\DateTime $createdDate): Location
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Location
     */
    public function setDeletedDate(?\DateTime $deletedDate): Location
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Location
     */
    public function setModifiedDate(\DateTime $modifiedDate): Location
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Location
     */
    public function setCreatedBy(string $createdBy): Location
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Location
     */
    public function setModifiedBy(string $modifiedBy): Location
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Location
     */
    public function setDeletedBy(?string $deletedBy): Location
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDeactivated()
    {
        return $this->deactivated;
    }

    /**
     * @param bool|null $deactivated
     * @return location
     */
    public function setDeactivated(?bool $deactivated)
    {
        $this->deactivated = $deactivated;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubLocation()
    {
        return $this->subLocation;
    }

    /**
     * @param string $subLocation
     * @return Location
     */
    public function setSubLocation(string $subLocation): Location
    {
        $this->subLocation = $subLocation;
        return $this;
    }

    /**
     * @return \Company
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param \Company $companyId
     * @return Location
     */
    public function setCompanyId($companyId): Location
    {
        $this->companyId = $companyId;
        return $this;
    }

    public function __ToInt()
    {
        return $this->locationId;
    }
}
