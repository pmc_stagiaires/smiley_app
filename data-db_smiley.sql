-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Gegenereerd op: 16 jul 2019 om 15:08
-- Serverversie: 5.7.25
-- PHP-versie: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_smiley`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `answer`
--

CREATE TABLE `answer` (
  `answerId` int(11) NOT NULL,
  `weighting` int(11) NOT NULL,
  `answerType` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answerValue` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `answerNumber` int(11) NOT NULL,
  `answerImagePath` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `questionId` int(11) DEFAULT NULL,
  `surveyId` int(11) NOT NULL,
  `languageCode` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `answer`
--

INSERT INTO `answer` (`answerId`, `weighting`, `answerType`, `answerValue`, `answerNumber`, `answerImagePath`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `questionId`, `surveyId`, `languageCode`) VALUES
(1, 1, 'Closed', 'Man', 1, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 1, 1, NULL),
(2, 1, 'Closed', 'Woman', 2, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 1, 1, NULL),
(3, 1, 'Scale', 'Value', 3, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 2, 1, NULL),
(4, 1, 'MultipleChoice', 'Red', 4, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 3, 1, NULL),
(5, 1, 'MultipleChoice', 'Blue', 5, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 3, 1, NULL),
(6, 1, 'MultipleChoice', 'Yellow', 6, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 3, 1, NULL),
(7, 1, 'MultipleChoice', 'Orange', 7, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 3, 1, NULL),
(8, 1, 'MultipleChoice', 'Green', 8, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 3, 1, NULL),
(9, 1, 'OpenQuestion', 'Value', 9, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 4, 1, NULL),
(10, 1, 'ImagePath', 'Color template 1', 10, 'b49c27d5930fe2c50ba537e16af7a646.png', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-27', '2019-06-27', NULL, 5, 1, NULL),
(11, 1, 'ImagePath', 'Color template 2', 11, 'e21db5001e0935b2966ef78f3ea964f9.png', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-27', '2019-06-27', NULL, 5, 1, NULL),
(12, 1, 'ImagePath', 'Color template 3', 12, 'b7b758250871c5e969185e8afc8e517f.png', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-27', '2019-06-27', NULL, 5, 1, NULL),
(13, 1, 'Smiley', 'Smiley rating', 13, NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-27', '2019-06-27', NULL, 6, 1, NULL),
(14, 1, 'Smiley', '1', 14, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 7, 1, NULL),
(15, 1, 'MultipleChoice', '1', 1, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 10, 8, NULL),
(16, 1, 'Smiley', '1', 1, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 11, 7, NULL),
(17, 1, 'OpenQuestion', '1', 2, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 12, 7, NULL),
(19, 1, 'Smiley', '1', 1, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, NULL, 3, NULL),
(20, 1, 'Smiley', '1', 2, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 17, 3, NULL),
(21, 1, 'Smiley', '1', 1, NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 18, 4, NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `answeredpage`
--

CREATE TABLE `answeredpage` (
  `answeredPageId` int(11) NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  `answeredQuestionId` int(11) DEFAULT NULL,
  `sessionId` int(11) DEFAULT NULL,
  `surveyId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `answeredpage`
--

INSERT INTO `answeredpage` (`answeredPageId`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `pageId`, `answeredQuestionId`, `sessionId`, `surveyId`) VALUES
(1, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 1, 15, 1),
(2, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 2, 15, 1),
(3, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 3, 15, 1),
(4, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 2, 4, 15, 1),
(5, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 2, 5, 15, 1),
(6, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 2, 6, 15, 1),
(7, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 3, 7, 15, 1),
(8, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 8, 15, 1),
(9, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 9, 15, 1),
(10, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 10, 15, 1),
(11, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 2, 11, 15, 1),
(12, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 2, 12, 15, 1),
(13, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 2, 13, 15, 1),
(14, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 3, 14, 15, 1),
(15, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6, 15, 19, 7),
(16, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 27, 16, 19, 7),
(17, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6, 17, 20, 7),
(18, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 27, 18, 20, 7),
(19, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 29, 19, 31, 3);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `answeredquestion`
--

CREATE TABLE `answeredquestion` (
  `answeredQuestionId` int(11) NOT NULL,
  `answeredAnswerValue` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `answerId` int(11) DEFAULT NULL,
  `questionId` int(11) DEFAULT NULL,
  `sessionId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `answeredquestion`
--

INSERT INTO `answeredquestion` (`answeredQuestionId`, `answeredAnswerValue`, `deletedBy`, `createdBy`, `modifiedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `answerId`, `questionId`, `sessionId`) VALUES
(1, 'Man', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 1, 1, 15),
(2, 'Woman', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 2, 1, 15),
(3, '2', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 3, 2, 15),
(4, 'Red', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 4, 3, 15),
(5, 'Orange', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 7, 3, 15),
(6, 'Tennis', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 9, 4, 15),
(7, 'unhappy', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 13, 6, 15),
(8, 'Man', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 1, 1, 15),
(9, 'Woman', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 2, 1, 15),
(10, '2', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 3, 2, 15),
(11, 'Red', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 4, 3, 15),
(12, 'Orange', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 7, 3, 15),
(13, 'Tennis', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 9, 4, 15),
(14, 'unhappy', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-02', '2019-07-02', NULL, 13, 6, 15),
(15, 'meh', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-05', '2019-07-05', NULL, 16, 11, 19),
(16, 'dave', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-05', '2019-07-05', NULL, 17, 12, 19),
(17, 'happy', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-05', '2019-07-05', NULL, 16, 11, 20),
(18, 'dave', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-05', '2019-07-05', NULL, 17, 12, 20),
(19, 'meh', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', '2019-07-16', '2019-07-16', NULL, 20, 17, 31);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `answerlanguage`
--

CREATE TABLE `answerlanguage` (
  `languageCode` int(11) NOT NULL,
  `answerId` int(11) NOT NULL,
  `answerTitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answerValue` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `company`
--

CREATE TABLE `company` (
  `companyId` int(11) NOT NULL,
  `companyName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `company`
--

INSERT INTO `company` (`companyId`, `companyName`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`) VALUES
(1, 'Mediatastisch', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-06-26', '2019-06-26', NULL),
(2, 'Ponthus', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-06-26', '2019-06-26', NULL),
(5, 'asd', 'davediederen@gmail.com', NULL, NULL, '2019-07-16', '2019-07-16', NULL);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `device`
--

CREATE TABLE `device` (
  `deviceId` int(11) NOT NULL,
  `deviceLicense` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `modifiedDate` date NOT NULL,
  `locationId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `device`
--

INSERT INTO `device` (`deviceId`, `deviceLicense`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `deletedDate`, `modifiedDate`, `locationId`) VALUES
(1, '0edb4f11-0c34-43d5-a8af', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-05-27', NULL, '2019-05-27', 2),
(2, '8adb2f5c-fc34-43d5-a8af', 'kevin.stevelmans35@gmail.com', 'davediederen@gmail.com', NULL, '2019-05-27', NULL, '2019-07-16', 2),
(3, '8adb2a3c-fc64-42d5-a8af', 'kevin.stevelmans35@gmail.com', 'davediederen@gmail.com', NULL, '2019-06-21', NULL, '2019-07-16', 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `location`
--

CREATE TABLE `location` (
  `locationId` int(11) NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postalCode` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `companyId` int(11) DEFAULT NULL,
  `subLocation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `location`
--

INSERT INTO `location` (`locationId`, `address`, `postalCode`, `city`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `companyId`, `subLocation`) VALUES
(1, 'Berkenlaan 52', '6133 WZ', 'Sittard', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 2, 'Balie, Vlees'),
(2, 'Pastoor Cramerstraat 2D', '6102 AC', 'Echt', 'kevin.stevelmans35@gmail.com', 'davediederen@gmail.com', NULL, '2019-06-26', '2019-07-16', NULL, 1, 'Entree');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190524121500', '2019-05-24 12:15:15'),
('20190524125015', '2019-05-24 12:50:35'),
('20190524132301', '2019-05-24 13:23:52'),
('20190524134714', '2019-05-24 13:47:23'),
('20190524135556', '2019-05-24 13:56:29'),
('20190524135616', '2019-05-24 13:56:29'),
('20190524142503', '2019-05-24 14:25:09'),
('20190606114709', '2019-06-18 08:21:57');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `page`
--

CREATE TABLE `page` (
  `pageId` int(11) NOT NULL,
  `pageNumber` int(11) NOT NULL,
  `pageTitle` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `previousPage` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nextPage` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `surveyId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `page`
--

INSERT INTO `page` (`pageId`, `pageNumber`, `pageTitle`, `previousPage`, `nextPage`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `surveyId`) VALUES
(1, 1, 'Page 1', '0', '2', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 1),
(2, 2, 'Page 2', '1', '3', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 1),
(3, 3, 'Page 3', '2', '4', 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, 1),
(5, 4, 'test page na update', '3', '0', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1),
(6, 1, '2', '0', '2', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 7),
(7, 1, '123', '0', '0', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 8),
(10, 1, '1', '0', '2', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 4),
(11, 1, '1', '0', '2', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(12, 2, '1', '1', '3', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(13, 3, 'e', '2', '4', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(14, 4, 'e', '3', '5', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(15, 5, '2', '4', '6', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(16, 6, '2', '5', '7', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(17, 7, '12', '6', '0', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 5),
(18, 1, '2', '0', '2', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(19, 2, '1', '1', '3', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(20, 3, '123', '2', '4', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(21, 4, '2', '3', '5', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(22, 5, '2', '4', '6', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(23, 6, 'e', '5', '7', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(24, 7, 'e', '6', '8', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(25, 8, 'e', '7', '9', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(26, 9, 'e', '8', '0', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 6),
(27, 2, '1', '1', '0', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 7),
(30, 1, '2', '0', '2', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 3),
(31, 2, '2', '1', '3', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 3),
(32, 2, 'Final page', '1', '0', 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `question`
--

CREATE TABLE `question` (
  `questionId` int(11) NOT NULL,
  `questionNumber` int(11) NOT NULL,
  `questionTitle` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deactivated` tinyint(1) DEFAULT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `answerId` int(11) DEFAULT NULL,
  `languageCode` int(11) DEFAULT NULL,
  `pageId` int(11) DEFAULT NULL,
  `surveyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `question`
--

INSERT INTO `question` (`questionId`, `questionNumber`, `questionTitle`, `deactivated`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `answerId`, `languageCode`, `pageId`, `surveyId`) VALUES
(1, 1, 'What is your gender?', NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, NULL, NULL, 1, 1),
(2, 2, 'Choose a number between 1 and 5', NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, NULL, NULL, 1, 1),
(3, 1, 'What is your favorite color?', NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, NULL, NULL, 2, 1),
(4, 2, 'What is your favorite hobby?', NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, NULL, NULL, 2, 1),
(5, 3, 'Which color template do you like the most?', NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, NULL, NULL, 2, 1),
(6, 1, 'Which rating would you give for this survey?', NULL, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-06-26', '2019-06-26', NULL, NULL, NULL, 3, 1),
(7, 1, 'asdasd', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, NULL, NULL, 5, 1),
(10, 1, 'Vas', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, NULL, NULL, 7, 8),
(11, 1, 'Vas', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, NULL, NULL, 6, 7),
(12, 1, 'Vas', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, NULL, NULL, 27, 7),
(17, 1, 'Vas', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, NULL, NULL, 29, 3),
(18, 1, 'Vas', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, NULL, NULL, 10, 4),
(19, 1, 'Vas', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, NULL, NULL, 17, 5),
(20, 1, 'Hoe tevreden ben je over jezelf', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, NULL, NULL, 32, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `questionlanguage`
--

CREATE TABLE `questionlanguage` (
  `languageCode` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `questionTitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `questionDescription` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `questionValue` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `session`
--

CREATE TABLE `session` (
  `sessionId` int(11) NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `deviceId` int(11) DEFAULT NULL,
  `surveyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `session`
--

INSERT INTO `session` (`sessionId`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `deviceId`, `surveyId`) VALUES
(1, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 3),
(2, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 4),
(3, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 1),
(4, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 4),
(5, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 1),
(6, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 1),
(7, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 4),
(8, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 5),
(9, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 5),
(10, 'kevin.stevelmans35@gmail.com', 'kevin.stevelmans35@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 4),
(11, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 5),
(12, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 4),
(13, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 3),
(14, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 1),
(15, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-02', '2019-07-02', NULL, 1, 1),
(16, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 1, 3),
(17, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 1, 7),
(18, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 1, 7),
(19, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 1, 7),
(20, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-05', '2019-07-05', NULL, 1, 7),
(21, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(22, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(23, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(24, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(25, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(26, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(27, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(28, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(29, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(30, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(31, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(32, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(33, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(34, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(35, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(36, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(37, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(38, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 3),
(39, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4),
(40, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4),
(41, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4),
(42, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4),
(43, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4),
(44, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 5),
(45, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4),
(46, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `survey`
--

CREATE TABLE `survey` (
  `surveyId` int(11) NOT NULL,
  `surveyTitle` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surveyDescription` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surveyVersion` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surveyBackgroundPath` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `companyId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `survey`
--

INSERT INTO `survey` (`surveyId`, `surveyTitle`, `surveyDescription`, `surveyVersion`, `surveyBackgroundPath`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `companyId`) VALUES
(4, 'Survey', 'te', '1', NULL, 'davediederen@gmail.com', 'davediederen@gmail.com', NULL, '2019-07-16', '2019-07-16', NULL, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `surveyresult`
--

CREATE TABLE `surveyresult` (
  `surveyResultsId` int(11) NOT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `answeredPageIds` int(11) NOT NULL,
  `deviceId` int(11) DEFAULT NULL,
  `locationId` int(11) DEFAULT NULL,
  `sessionId` int(11) DEFAULT NULL,
  `surveyId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `userId` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activationCode` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modifiedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deletedBy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `createdDate` date DEFAULT NULL,
  `modifiedDate` date DEFAULT NULL,
  `deletedDate` date DEFAULT NULL,
  `deactivated` tinyint(1) DEFAULT NULL,
  `companyId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`userId`, `email`, `password`, `activationCode`, `role`, `createdBy`, `modifiedBy`, `deletedBy`, `createdDate`, `modifiedDate`, `deletedDate`, `deactivated`, `companyId`) VALUES
(1, 'kevin.stevelmans35@gmail.com', '$2y$12$WBapsX4biwvlDrMvlvU5eOZHgTNvyo7GoJUD4q9OvSF5.u6t12QcC', '28c95d5e-3f0c-474c-bc8f-549436bc3f74', 'ROLE_SUPER_ADMIN', 'anon.', NULL, NULL, '2019-05-28', NULL, NULL, NULL, 1),
(2, 'jk.otten@outlook.com', '$2y$12$PMoTFICT6pd/Zs7gYie59uyZABpXHWvGfegsQTjy7cOVrU5ruBSlW', '2fcc28bc-38f6-4886-9a28-47b5c6dd36c2', 'ROLE_ADMIN', 'anon.', NULL, NULL, '2019-05-27', NULL, NULL, NULL, 2),
(3, 'gebruiker@homail.com', '$2y$12$mH2rGev2ClmURVarywI62.ph9Ag3QXJA2o5DbISJbPFPDSelYW9i2', '24ca5940-36f6-4a96-b573-b96f97690fa3', 'ROLE_USER', 'jk.otten@outlook.com', NULL, NULL, '2019-05-27', NULL, NULL, NULL, 2),
(4, 'gebruiker2@gmail.com', '$2y$12$66XVLfZxe0XUxlIPVnCNhuzKdjxXSRFVhnRnBd4Dbnhh99kkgGPe2', '9bfc6c5b-8eb6-4f76-95fd-f1e5906e374a', 'ROLE_ADMIN ', 'jk.otten@outlook.com', NULL, NULL, '2019-05-27', NULL, NULL, NULL, 2),
(5, 'test@mail.com', '$2y$12$rKVFfZ5UUdBTGoyYV7HD7eEbO7or1Cm7hTPGwOdUDwlJlpPjmgwt6', '341441a4-c7eb-47ba-8df6-c38ca9db33af', 'ROLE_USER', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-05-28', NULL, NULL, NULL, 2),
(6, 'user@mail.com', '$2y$12$MDvP9CEX9pmgh9J.Ijqkp.Eet2fWpjlwolv6QyWCY6TO0H9F1kMhi', '66645044-a896-4f07-9c32-7bb76904806b', 'ROLE_USER', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-05-29', NULL, NULL, NULL, 2),
(7, 'davediederen@gmail.com', '$2y$13$p7vdS10Zw.jJ9y7nFfwv.u9JzbGaYtN429uCeiJtJk.fVuhMEjcKW', '07692f8a-e65f-4a9c-b29d-ad9eef4b9724', 'ROLE_SUPER_ADMIN', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-06-21', NULL, NULL, NULL, 1),
(8, 'glenn@mediatastisch.nl', '$2y$13$4cghsnQSpvsxUnjO8EioueRLy8cy7ZPztia7t9p6c6F61lnQt/Ezq', 'f2e64995-62d2-44a5-bb1d-83f855bbf467', 'ROLE_SUPER_ADMIN', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-06-21', NULL, NULL, NULL, 1),
(9, 'dennis@superemail.com', '$2y$12$sNWCHcoxGBL0KrO/fT3jhOgG.jb2kkdZcr7MEuh1BQK1Fn0.wmxTq', '8c9a1abc-05c0-478a-8cce-c380f828f9e7', 'ROLE_SUPER_ADMIN', 'kevin.stevelmans35@gmail.com', NULL, NULL, '2019-07-02', NULL, NULL, NULL, 1),
(10, 'dave@mediatastisch.nl', '$2y$13$9.snYPEZ.r5fAsq02EglSODzjbYNbt1cvxCPym5Ihh6Zc/bGmUmrm', '123b8baf-60b1-417c-8dec-a353c3787181', 'ROLE_USER', 'davediederen@gmail.com', NULL, NULL, '2019-07-16', NULL, NULL, NULL, 1);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`answerId`),
  ADD KEY `Language` (`languageCode`),
  ADD KEY `AnswerQuestion` (`questionId`),
  ADD KEY `surveyId` (`surveyId`);

--
-- Indexen voor tabel `answeredpage`
--
ALTER TABLE `answeredpage`
  ADD PRIMARY KEY (`answeredPageId`),
  ADD KEY `AnswerPageQuestion` (`answeredQuestionId`),
  ADD KEY `AnswerPageSession` (`sessionId`),
  ADD KEY `AnswerPagePage` (`pageId`),
  ADD KEY `AnswerPageSurvey` (`surveyId`);

--
-- Indexen voor tabel `answeredquestion`
--
ALTER TABLE `answeredquestion`
  ADD PRIMARY KEY (`answeredQuestionId`),
  ADD KEY `AnsweredQuestionQuestion` (`questionId`),
  ADD KEY `AnsweredQuestionSession` (`sessionId`),
  ADD KEY `AnsweredQuestionAnswer` (`answerId`);

--
-- Indexen voor tabel `answerlanguage`
--
ALTER TABLE `answerlanguage`
  ADD PRIMARY KEY (`languageCode`,`answerId`);

--
-- Indexen voor tabel `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`companyId`);

--
-- Indexen voor tabel `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`deviceId`),
  ADD KEY `DeviceLocation` (`locationId`);

--
-- Indexen voor tabel `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`locationId`),
  ADD KEY `CompanyLocation` (`companyId`);

--
-- Indexen voor tabel `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexen voor tabel `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pageId`),
  ADD KEY `pageSurvey` (`surveyId`);

--
-- Indexen voor tabel `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`questionId`);

--
-- Indexen voor tabel `questionlanguage`
--
ALTER TABLE `questionlanguage`
  ADD PRIMARY KEY (`languageCode`,`questionId`);

--
-- Indexen voor tabel `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`sessionId`),
  ADD KEY `SessionDevice` (`deviceId`),
  ADD KEY `surveyId` (`surveyId`);

--
-- Indexen voor tabel `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`surveyId`),
  ADD KEY `SurveyCompany` (`companyId`) USING BTREE;

--
-- Indexen voor tabel `surveyresult`
--
ALTER TABLE `surveyresult`
  ADD PRIMARY KEY (`surveyResultsId`),
  ADD KEY `surveyResultLocation` (`locationId`),
  ADD KEY `surveyResultSession` (`sessionId`),
  ADD KEY `surveyResultDevice` (`deviceId`),
  ADD KEY `surveyResultSurvey` (`surveyId`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `answer`
--
ALTER TABLE `answer`
  MODIFY `answerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT voor een tabel `answeredpage`
--
ALTER TABLE `answeredpage`
  MODIFY `answeredPageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT voor een tabel `answeredquestion`
--
ALTER TABLE `answeredquestion`
  MODIFY `answeredQuestionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT voor een tabel `company`
--
ALTER TABLE `company`
  MODIFY `companyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `device`
--
ALTER TABLE `device`
  MODIFY `deviceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT voor een tabel `location`
--
ALTER TABLE `location`
  MODIFY `locationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT voor een tabel `page`
--
ALTER TABLE `page`
  MODIFY `pageId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT voor een tabel `question`
--
ALTER TABLE `question`
  MODIFY `questionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT voor een tabel `session`
--
ALTER TABLE `session`
  MODIFY `sessionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT voor een tabel `survey`
--
ALTER TABLE `survey`
  MODIFY `surveyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
