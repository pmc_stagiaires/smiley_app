<?php

namespace App\Repository;

use App\Entity\Surveyquestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Surveyquestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Surveyquestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Surveyquestion[]    findAll()
 * @method Surveyquestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SurveyquestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Surveyquestion::class);
    }

    // /**
    //  * @return Surveyquestion[] Returns an array of Surveyquestion objects
    //  */
    /*
    public_html function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public_html function findOneBySomeField($value): ?Surveyquestion
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
