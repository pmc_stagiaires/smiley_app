/**
 * Theme: Highdmin - Responsive Bootstrap 4 Admin Dashboard
 * Author: Coderthemes
 * Component: Companies
 *
 */

//TODO: De company sparklines zijn statisch, dit zijn de gegevens voor de ingevulde surveys en de gebruikte devices op de manager homepagina. (MOET NOG DYNAMISCH WORDEN)

$( document ).ready(function() {
    var DrawSparkline = function() {
        $('.company-1').sparkline([0, 23, 100, 35, 44, 45, 56, 37, 40], {
            type: 'line',
            width: "100%",
            height: '80',
            chartRangeMax: 50,
            lineColor: '#02c0ce',
            fillColor: 'rgba(2, 192, 206, 0.1)',
            highlightLineColor: 'rgba(0,0,0,.1)',
            highlightSpotColor: 'rgba(0,0,0,.2)',
            maxSpotColor: false,
            minSpotColor: false,
            spotColor: false,
            lineWidth: 1
        });
        $('.company-2').sparkline([0, 25, 48, 32, 36, 20, 85, 56, 36],{
            type: 'line',
            width: "100%",
            height: '80',
            chartRangeMax: 50,
            lineColor: '#02c0ce',
            fillColor: 'rgba(2, 192, 206, 0.1)',
            highlightLineColor: 'rgba(0,0,0,.1)',
            highlightSpotColor: 'rgba(0,0,0,.2)',
            maxSpotColor: false,
            minSpotColor: false,
            spotColor: false,
            lineWidth: 1
        });
    };

    DrawSparkline();

    var resizeChart;

    $(window).resize(function(e) {
        clearTimeout(resizeChart);
        resizeChart = setTimeout(function() {
            DrawSparkline();
        }, 300);
    });
});