<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionLanguage
 *
 * @ORM\Table(name="questionlanguage")
 * @ORM\Entity
 */
class QuestionLanguage {
    /**
     * @var int
     *
     * @ORM\Column(name="languageCode", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageCode;

    /**
     * @var int
     *
     * @ORM\Column(name="questionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $questionId;

    /**
     * @var string
     *
     * @ORM\Column(name="questionTitle", type="string", length=100, nullable=false)
     */
    private $questionTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="questionDescription", type="string", length=250, nullable=true)
     */
    private $questionDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="questionValue", type="string", length=100, nullable=true)
     */
    private $questionValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @return int
     */
    public function getLanguageCode(): int
    {
        return $this->languageCode;
    }

    /**
     * @param int $languageCode
     * @return QuestionLanguage
     */
    public function setLanguageCode(int $languageCode): QuestionLanguage
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     * @return QuestionLanguage
     */
    public function setQuestionId(int $questionId): QuestionLanguage
    {
        $this->questionId = $questionId;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionTitle(): string
    {
        return $this->questionTitle;
    }

    /**
     * @param string $questionTitle
     * @return QuestionLanguage
     */
    public function setQuestionTitle(string $questionTitle): QuestionLanguage
    {
        $this->questionTitle = $questionTitle;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getQuestionDescription(): ?string
    {
        return $this->questionDescription;
    }

    /**
     * @param null|string $questionDescription
     * @return QuestionLanguage
     */
    public function setQuestionDescription(?string $questionDescription): QuestionLanguage
    {
        $this->questionDescription = $questionDescription;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getQuestionValue(): ?string
    {
        return $this->questionValue;
    }

    /**
     * @param null|string $questionValue
     * @return QuestionLanguage
     */
    public function setQuestionValue(?string $questionValue): QuestionLanguage
    {
        $this->questionValue = $questionValue;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return QuestionLanguage
     */
    public function setCreatedDate(\DateTime $createdDate): QuestionLanguage
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return QuestionLanguage
     */
    public function setModifiedDate(\DateTime $modifiedDate): QuestionLanguage
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return QuestionLanguage
     */
    public function setDeletedDate(?\DateTime $deletedDate): QuestionLanguage
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return QuestionLanguage
     */
    public function setCreatedBy(string $createdBy): QuestionLanguage
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return QuestionLanguage
     */
    public function setModifiedBy(string $modifiedBy): QuestionLanguage
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return QuestionLanguage
     */
    public function setDeletedBy(?string $deletedBy): QuestionLanguage
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }
}
