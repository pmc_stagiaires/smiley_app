<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * surveyResult
 *
 * @ORM\Table(name="surveyresult", indexes={@ORM\Index(name="surveyResultLocation", columns={"locationId"}), @ORM\Index(name="surveyResultSession", columns={"sessionId"}), @ORM\Index(name="surveyResultDevice", columns={"deviceId"}), @ORM\Index(name="surveyResultSurvey", columns={"surveyId"})})
 * @ORM\Entity
 */
class SurveyResult {
    /**
     * @var int
     *
     * @ORM\Column(name="surveyResultsId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $surveyResultsId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="answeredPageIds", type="string", nullable=false)
     */
    private $answeredPageIds;

    /**
     * @var \Device
     *
     * @ORM\ManyToOne(targetEntity="Device")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="deviceId", referencedColumnName="deviceId")
     * })
     */
    private $deviceId;

    /**
     * @var \Location
     *
     * @ORM\ManyToOne(targetEntity="Location")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="locationId", referencedColumnName="locationId")
     * })
     */
    private $locationId;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sessionId", referencedColumnName="sessionId")
     * })
     */
    private $sessionId;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @return int
     */
    public function getsurveyResultsId()
    {
        return $this->surveyResultsId;
    }

    /**
     * @param int $surveyResultsId
     * @return surveyResult
     */
    public function setsurveyResultsId(int $surveyResultsId)
    {
        $this->surveyResultsId = $surveyResultsId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return surveyResult
     */
    public function setCreatedDate(\DateTime $createdDate): surveyResult
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return surveyResult
     */
    public function setModifiedDate(\DateTime $modifiedDate): surveyResult
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return surveyResult
     */
    public function setDeletedDate(?\DateTime $deletedDate): surveyResult
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return surveyResult
     */
    public function setCreatedBy(string $createdBy): surveyResult
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return surveyResult
     */
    public function setModifiedBy(string $modifiedBy): surveyResult
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return surveyResult
     */
    public function setDeletedBy(?string $deletedBy): surveyResult
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnsweredPageIds(): string
    {
        return $this->answeredPageIds;
    }

    /**
     * @param $answeredPageIds
     * @return surveyResult
     */
    public function setAnsweredPageIds($answeredPageIds)
    {
        $this->answeredPageIds = $answeredPageIds;
        return $this;
    }

    /**
     * @return \Device
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param $deviceId
     * @return surveyResult
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return \Location
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param $locationId
     * @return surveyResult
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
        return $this;
    }

    /**
     * @return \Session
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param $sessionId
     * @return surveyResult
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return \Survey
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param $surveyId
     * @return surveyResult
     */
    public function setSurveyId($surveyId)
    {
        $this->surveyId = $surveyId;
        return $this;
    }
}
