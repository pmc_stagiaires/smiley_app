<?php


namespace App\Form\Device;

use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Survey;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class EditDeviceFormType extends AbstractType
{
//    public function buildForm(FormBuilderInterface $builder, array $options)
//    {
//        $builder
//            ->add('deviceLicense', TextType::class,
//                array('attr' => array('class' => 'form-control')))
//            ->add('locationId', EntityType::class, ['class' => Location::class,
//                'choice_label' => function(Location $device) {
//                    return sprintf( $device->getAddress(), $device->getAddress());
//                },
//                'placeholder' => 'Choose an location',
//                'attr' => ['class' => 'form-control']
//            ])
//            ->add('isActive', CheckboxType::class,[
//                'label' => 'Set device active ',
//                'required' => false,
//                'attr' => ['class' => 'checkbox']
//            ])
//            ->add('save', SubmitType::class, array('label' => 'Update', 'attr' => array('class' => 'btn btn-primary
//                mt-3')))->getForm();
//    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Device::class,
        ]);
    }
}