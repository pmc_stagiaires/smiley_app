<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface {

    /**
     * @var int
     *
     * @ORM\Column(name="userId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60, nullable=false)
     */
    private $password;

    private $currentPassword;

    private $newPassword;

    /**
     * @var string
     *
     * @ORM\Column(name="activationCode", type="string", length=50, nullable=false)
     */
    private $activationCode;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="deactivated", type="boolean", nullable=true)
     */
    private $deactivated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=true)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=true)
     */
    private $modifiedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=true)
     */
    private $createdBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=true)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @ORM\Column(name="role", type="string", length=50, nullable=true)
     *
     */
    private $role;

    /**
     * @var \Company
     *
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companyId", referencedColumnName="companyId")
     * })
     */
    private $companyId;

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return User
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return array($this->role);
    }

    /**
     * @param mixed $role
     * @return User
     */
    public function setRoles($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * @param mixed $password
     * @return mixed
     */
    public function setCurrentPassword($password)
    {
        $this->currentPassword = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     * @return User
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * @param string $activationCode
     * @return User
     */
    public function setActivationCode(string $activationCode)
    {
        $this->activationCode = $activationCode;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDeactivated()
    {
        return $this->deactivated;
    }

    /**
     * @param bool|null $deactivated
     * @return User
     */
    public function setDeactivated(?bool $deactivated)
    {
        $this->deactivated = $deactivated;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return User
     */
    public function setCreatedDate(\DateTime $createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate()
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return User
     */
    public function setDeletedDate(?\DateTime $deletedDate)
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime|null $modifiedDate
     * @return User
     */
    public function setModifiedDate(?\DateTime $modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return User
     */
    public function setCreatedBy(string $createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * @param string|null $modifiedBy
     * @return User
     */
    public function setModifiedBy(?string $modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * @param string|null $deletedBy
     * @return User
     */
    public function setDeletedBy(?string $deletedBy)
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * We use Bcrypt which generates it's own salt per user
     * Don't delete this function though! UserInterface doesn't like that :(
     */
    public function getSalt(){}

    /**
     * Returns the username used to authenticate the user.
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(){}

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->userId,
            $this->email,
            $this->password,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->userId,
            $this->email,
            $this->password,
            ) = unserialize($serialized);
    }

    /**
     * @return \Company
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * @param \Company $companyId
     * @return User
     */
    public function setCompanyId($companyId): User
    {
        $this->companyId = $companyId;
        return $this;
    }
}