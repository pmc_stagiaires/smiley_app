<?php

namespace App\Controller;


use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Survey;
use App\Helper\Validation;
use App\Repository\DeviceRepository;
use App\Repository\SurveySurveyRepository;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Helper\ResponseBuilder;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\View;


/**
 * @property TokenStorageInterface tokenStorage
 */
class ApiController extends AbstractController
{

    public function getCurrentUser()
    {
        $currentUser = $this->getUser();

        if ($currentUser) {
            return $currentUser;
        } else {
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route(path="/devices/survey", name="device_survey")
     * @param Request $request
     * @return Response
     */
    public function getSurvey(Request $request)
    {
        $locations = $this->getDoctrine()->getRepository(Location::class)->findAll();
        $user = $this->getDoctrine()->getRepository(Device::class)->findBy(['locationId' => $locations]);
        $devices = $this->getDoctrine()->getRepository(Device::class)->findBy(['locationId' => $user]);
        $surveys = $this->getDoctrine()->getRepository(Survey::class)->findAll();

        if (!$user) {
            return new Response(ResponseBuilder::build([], "401", "Invalid auth token"), 401);
        }
        $surveyList = [];
        foreach ($surveys as $survey) {
            /**
             * @var $item Device
             */
            $surveyList[] = [
                "id" => $survey->getSurveyId(),
                "Survey Title" => $survey->getSurveyTitle(),
                "Company" => $survey->getCompanyId()->getCompanyName(),
            ];
        }
        return new Response(ResponseBuilder::build($surveyList));
    }


    /**
     * @Rest\Post("/enroll/device")
     * @param Request $request
     * @param DeviceRepository $deviceRepository
     * @param $deviceId
     * @return Response
     * @throws \Exception
     */

    public function enrollDevice(Request $request, DeviceRepository $deviceRepository)
    {

        $response = json_decode($request->getContent());

        $devices = $deviceRepository
            ->createQueryBuilder('d')
            ->where('d.uniqueDeviceId = :id')
            ->setParameter('id', $response->uniqueDeviceId)
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
            ->getArrayResult();

        if ($devices == null) {

            $device = new device();

            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
            $randomLicenseKey = substr(str_shuffle($chars), 0, 5);
//        $device->setUniqueDeviceId($request->get('uniqueDeviceId'));

            $device->setDeviceLicense($randomLicenseKey);
            $device->setUniqueDeviceId($response->uniqueDeviceId);
            $device->setCreatedDate(new \DateTime());
            $device->setModifiedDate(new \DateTime());
            $device->setModifiedBy('undefined');
            $device->setCreatedBy('undefined');
            $device->setIsActive(0);

            $this->getDoctrine()->getManager()->persist($device);
            $this->getDoctrine()->getManager()->flush();

            $newDevice = [
                'id' => $device->getDeviceId(),
                'deviceLicense' => $device->getDeviceLicense(),
                'uniqueDeviceId' => $device->getUniqueDeviceId(),
                'createdDate' => $device->getCreatedDate(),
                'deletedDate' => $device->getDeletedDate(),
                'deactivated' => $device->getDeactivated(),
                'modifiedDate' => $device->getModifiedDate(),
                'createdBy' => $device->getCreatedBy(),
                'modifiedBy' => $device->getModifiedBy(),
                'deletedBy' => $device->getDeletedBy(),
                'isActive' => $device->getIsActive(),
                'survey' => $device->getSurveyId()
            ];


            return new Response(ResponseBuilder::build($newDevice, 201));
        } else {
            return new Response(ResponseBuilder::build($devices[0], 200));
        }
    }

    /**
     * Retrieves an Article resource
     * @Rest\Get("/get/device/{deviceId}")
     * @param null $deviceId
     * @param DeviceRepository $deviceRepository
     * @return JsonResponse
     */
    public function getDevices($deviceId = null, DeviceRepository $deviceRepository)
    {
        $device = $deviceRepository
            ->createQueryBuilder('d')
            ->where('d.deviceId = :id')
            ->setParameter('id', $deviceId)
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
            ->getArrayResult();

//        dd($device);
        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return new JsonResponse($device[0], 200);
    }

    /**
     * Retrieves an Article resource
     * @Rest\Get("/get/survey/{surveyId}")
     * @param null $surveyId
     * @param SurveySurveyRepository $surveyRepository
     * @return JsonResponse
     */
    public function getSurveys($surveyId = null, SurveySurveyRepository $surveyRepository)
    {
        $survey = $surveyRepository
            ->createQueryBuilder('s')
            ->where('s.surveyId = :id')
            ->setParameter('id', $surveyId)
            ->getQuery()
            ->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)
            ->getArrayResult();

//        dd($device);
        // In case our GET was a success we need to return a 200 HTTP OK response with the request object
        return new JsonResponse($survey[0], 200);
    }

    /**
     * Replaces Article resource
     * @Rest\Put("/update/device/{deviceId}")
     * @param DeviceRepository $deviceRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function putDevice(DeviceRepository $deviceRepository, Request $request)
    {
        $response = json_decode($request->getContent());

        $devices = $deviceRepository
            ->createQueryBuilder('d')
            ->where('d.deviceId = :id')
            ->setParameter('id', $response->deviceId)
            ->getQuery()
            ->getArrayResult();


        if ($devices) {

            $device = new device();

            $device->setLocationId($request->get('locationId'));

//            $this->deviceRepository->save($devices);
            dd($device);
            return new JsonResponse($device, 200);
        }
    }


    /**
     * @Route(path="/devices/test", name="hotel_get_list")
     * @param Request $request
     * @return Response
     */
    public function getDevice(Request $request)
    {
        $locations = $this->getDoctrine()->getRepository(Location::class)->findAll();
        $surveys = $this->getDoctrine()->getRepository(Survey::class)->findAll();
        $user = $this->getDoctrine()->getRepository(Device::class)->findBy(['locationId' => $locations]);

        if (!$user) {
            return new Response(ResponseBuilder::build([], "401", "Invalid auth token"), 401);
        }
        $devices = $this->getDoctrine()->getRepository(Device::class)->findBy(['locationId' => $user]);
        $devices = array_filter($devices, function ($item) use ($user) {
            return true;
        });
        $deviceList = [];
        foreach ($devices as $device) {
            /**
             * @var $device Device
             */
            $deviceList[] = [
                "id" => $device->getDeviceId(),
                "License key" => $device->getDeviceLicense(),
                "Company" => $device->getLocationId()->getCompanyId()->getCompanyName(),
//                "Survey" => $hotel->getLocationId()->getCompanyId()->getSurveyId(),
//                "address" => $hotelAddress->getStreet() . " " . $hotelAddress->getNumber() . ", " . $hotelAddress->getPostal() . ", " . $hotelAddress->getCity(),
//                "image" => file_get_contents("../storage/demo-image-base-64.txt"),
            ];

        }
        return new Response(ResponseBuilder::build($deviceList));

        $surveyList = [];
        foreach ($surveys as $survey) {
            /**
             * @var $device Hotel
             */
            $surveyList[] = [
                "id" => $survey->getSurveyId(),
                "Survey Title" => $survey->getSurveyTitle(),
                "Company" => $survey->getCompanyId()->getCompanyName(),
            ];
        }
        return new Response(ResponseBuilder::build($surveyList));
    }
}