<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question", indexes={@ORM\Index(name="QuestionPage", columns={"pageId"}), @ORM\Index(name="QuestionSurvey", columns={"surveyId"}), @ORM\Index(name="QuestionLanguage", columns={"languageCode"})})
 * @ORM\Entity
 */
class Question {
    /**
     * @var int
     *
     * @ORM\Column(name="questionId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $questionId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="deactivated", type="boolean", nullable=true)
     */
    private $deactivated;

    /**
     * @var int
     *
     * @ORM\Column(name="questionNumber", type="integer", nullable=false)
     */
    private $questionNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="answerId", type="integer", nullable=false)
     */
    private $answerId;

    /**
     * @var int
     *
     * @ORM\Column(name="weighting", type="integer", nullable=false)
     */
    private $weighting;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="questionTitle", type="string", length=25, nullable=false)
     */
    private $questionTitle;


    /**
     * @var string
     *
     * @ORM\Column(name="answerTypes", type="string", length=100, nullable=true)
     */
    private $answerTypes;

    /**
     * @var \QuestionLanguage
     *
     * @ORM\ManyToOne(targetEntity="QuestionLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="languageCode", referencedColumnName="languageCode")
     * })
     */
    private $languageCode;

    /**
     * @var \Page
     *
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pageId", referencedColumnName="pageId")
     * })
     */
    private $pageId;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @return int
     */
    public function getQuestionId(): int
    {
        return $this->questionId;
    }

    /**
     * @param int $questionId
     * @return Question
     */
    public function setQuestionId(int $questionId): Question
    {
        $this->questionId = $questionId;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDeactivated(): ?bool
    {
        return $this->deactivated;
    }

    /**
     * @param bool|null $deactivated
     * @return Question
     */
    public function setDeactivated(?bool $deactivated): Question
    {
        $this->deactivated = $deactivated;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuestionNumber()
    {
        return $this->questionNumber;
    }

    /**
     * @param int $questionNumber
     * @return Question
     */
    public function setQuestionNumber(int $questionNumber): Question
    {
        $this->questionNumber = $questionNumber;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Question
     */
    public function setCreatedDate(\DateTime $createdDate): Question
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Question
     */
    public function setModifiedDate(\DateTime $modifiedDate): Question
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Question
     */
    public function setDeletedDate(?\DateTime $deletedDate): Question
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Question
     */
    public function setCreatedBy(string $createdBy): Question
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeighting()
    {
        return $this->weighting;
    }

    /**
     * @param int $weighting
     * @return Question
     */
    public function setWeighting(int $weighting): Question
    {
        $this->weighting = $weighting;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Question
     */
    public function setModifiedBy(string $modifiedBy): Question
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Question
     */
    public function setDeletedBy(?string $deletedBy): Question
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionTitle()
    {
        return $this->questionTitle;
    }

    /**
     * @param string $questionTitle
     * @return Question
     */
    public function setQuestionTitle(string $questionTitle): Question
    {
        $this->questionTitle = $questionTitle;
        return $this;
    }

    /**
     * @return
     */
    public function getAnswerTypes()
    {
        return $this->answerTypes;
    }

    /**
     * @return int $answerId
     */
    public function getAnswerId()
    {
        return $this->answerId;
    }

    /**
     * @param $answerTypes
     * @return Question
     */
    public function setAnswerTypes($answerTypes): Question
    {
        $this->answerTypes = $answerTypes;
        return $this;
    }

    /**
     * @return
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param  $languageCode
     * @return Question
     */
    public function setLanguageCode( $languageCode): Question
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    /**
     * @return \Page
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param $pageId
     * @return Question
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     * @return \
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param  $surveyId
     * @return Question
     */
    public function setSurveyId($surveyId): Question
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    public function __toInt()
    {
        return $this->questionId;
    }
}
