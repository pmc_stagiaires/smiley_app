<?php


namespace App\Form\Location;

use App\Entity\Company;
use App\Entity\Location;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditLocationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('postalCode', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('city', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('subLocation', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control',
                    'label' => 'Sub locations'
                ]
            ])
            ->add('companyId', EntityType::class, [
                'attr' => [
                    'id' => 'sel1',
                    'class' => 'form-control'
                ], 'class' => Company::class, 'choice_label' => function(Company $company) {
                    return sprintf('(%d) %s', $company->getCompanyId() ,$company->getCompanyName());
                }
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Update', 'attr' => [
                    'class' => 'btn btn-primary mt-3'
                ]
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Location::class,
        ]);
    }
}