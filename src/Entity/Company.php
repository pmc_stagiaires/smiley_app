<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 *
 * @ORM\Table(name="company", indexes={@ORM\Index(name="CompanyUser", columns={"userId"})})
 * @ORM\Entity
 */
class Company {
    /**
     * @var int
     *
     * @ORM\Column(name="companyId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $companyId;

    /**
     * @var string
     *
     * @ORM\Column(name="companyName", type="string", length=100, nullable=false)
     */
    private $companyName;

    /**
     * @var int
     *
     * @ORM\Column(name="userId", type="string", length=100, nullable=false)
     */
    private $userId;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="deactivated", type="boolean", nullable=true)
     */
    private $deactivated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=true)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return Company
     */
    public function setCompanyId(int $companyId): Company
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     * @return Company
     */
    public function setCompanyName(string $companyName): Company
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Company
     */
    public function setCreatedDate(\DateTime $createdDate): Company
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Company
     */
    public function setDeletedDate(?\DateTime $deletedDate): Company
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getDeactivated()
    {
        return $this->deactivated;
    }

    /**
     * @param bool|null $deactivated
     * @return device
     */
    public function setDeactivated(?bool $deactivated)
    {
        $this->deactivated = $deactivated;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Company
     */
    public function setModifiedDate(\DateTime $modifiedDate): Company
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Company
     */
    public function setCreatedBy(string $createdBy): Company
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getModifiedBy(): ?string
    {
        return $this->modifiedBy;
    }

    /**
     * @param null|string $modifiedBy
     * @return Company
     */
    public function setModifiedBy(?string $modifiedBy): Company
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Company
     */
    public function setDeletedBy(?string $deletedBy): Company
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }
}