<?php

namespace App\Helper;

use Symfony\Component\HttpFoundation\Response;
class ResponseBuilder {
    public static function build($data, $status = 200, $message = ""){
        $response = new Response();
        $response->headers->add(['Content-Type' => 'application/json']);
        $returnObject = new \stdClass();
        $returnObject->status = $status;
        $returnObject->message = $message;
        $returnObject->data = $data;
        $response->setContent(json_encode($returnObject));
        $response->setStatusCode($status);
        return $response;
    }
}
