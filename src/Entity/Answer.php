<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * Answer
 *
 * @ORM\Table(name="answer", indexes={@ORM\Index(name="Language", columns={"languageCode"}), @ORM\Index(name="AnswerQuestion", columns={"questionId"})})
 * @ORM\Entity
 */
class Answer {
    /**
     * @var int
     *
     * @ORM\Column(name="answerId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $answerId;

    /**
     * @var int
     *
     * @ORM\Column(name="weighting", type="integer", nullable=false)
     */
    private $weighting;

    /**
     * @var string
     *
     * @ORM\Column(name="answerType", type="string", length=25, nullable=false)
     */
    private $answerType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answerValue", type="string", length=100, nullable=true)
     */
    private $answerValue;

    /**
     * @var int
     *
     * @ORM\Column(name="answerNumber", type="integer", nullable=false)
     */
    private $answerNumber;

//    /**
//     * @var string
//     *
//     * @ORM\Column(name="nextQuestion", type="string", nullable=true)
//     */
//    private $nextQuestion;


    /**
     * @var \Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nextQuestion", referencedColumnName="questionId")
     * })
     */
    private $nextQuestion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answerImagePath", type="string", length=150, nullable=true)
     */
    private $answerImagePath;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var \Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="questionId", referencedColumnName="questionId")
     * })
     */
    private $questionId;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @var \AnswerLanguage
     *
     * @ORM\ManyToOne(targetEntity="AnswerLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="languageCode", referencedColumnName="languageCode")
     * })
     */
    private $languageCode;

    /**
     * @return int
     */
    public function getAnswerId(): int
    {
        return $this->answerId;
    }

    /**
     * @param int $answerId
     * @return Answer
     */
    public function setAnswerId(int $answerId): Answer
    {
        $this->answerId = $answerId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeighting()
    {
        return $this->weighting;
    }

    /**
     * @param int $weighting
     * @return Answer
     */
    public function setWeighting(int $weighting): Answer
    {
        $this->weighting = $weighting;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswerType()
    {
        return $this->answerType;
    }

    /**
     * @param string $answerType
     * @return Answer
     */
    public function setAnswerType(string $answerType): Answer
    {
        $this->answerType = $answerType;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAnswerValue(): ?string
    {
        return $this->answerValue;
    }

    /**
     * @param null|string $answerValue
     * @return Answer
     */
    public function setAnswerValue(?string $answerValue): Answer
    {
        $this->answerValue = $answerValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswerNumber()
    {
        return $this->answerNumber;
    }

    /**
     * @param string $answerNumber
     * @return Answer
     */
    public function setAnswerNumber(string $answerNumber): Answer
    {
        $this->answerNumber = $answerNumber;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNextQuestion()
    {
        return $this->nextQuestion;
    }

    /**
     * @param null|$nextQuestion
     * @return Answer
     */
    public function setNextQuestion($nextQuestion): Answer
    {
        $this->nextQuestion = $nextQuestion;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getAnswerImagePath()
    {
        return $this->answerImagePath;
    }

    /**
     * @param null|string $answerImagePath
     * @return Answer
     */
    public function setAnswerImagePath(?string $answerImagePath)
    {
        $this->answerImagePath = $answerImagePath;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param DateTime $createdDate
     * @return Answer
     */
    public function setCreatedDate(DateTime $createdDate): Answer
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModifiedDate(): DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param DateTime $modifiedDate
     * @return Answer
     */
    public function setModifiedDate(DateTime $modifiedDate): Answer
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDeletedDate(): ?DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param DateTime|null $deletedDate
     * @return Answer
     */
    public function setDeletedDate(?DateTime $deletedDate): Answer
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Answer
     */
    public function setCreatedBy(string $createdBy): Answer
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Answer
     */
    public function setModifiedBy(string $modifiedBy): Answer
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Answer
     */
    public function setDeletedBy(?string $deletedBy): Answer
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return \Question
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * @param  $questionId
     * @return Answer
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;
        return $this;
    }

    /**
     * @return \Survey
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param  $surveyId
     * @return Answer
     */
    public function setSurveyId($surveyId): Answer
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    /**\AnswerLanguage
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param \AnswerLanguage $languageCode
     * @return Answer
     */
    public function setLanguageCode( $languageCode): Answer
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    public function __toInt()
    {
        return $this->answerId;
    }

    public function __toString()
    {
        return (string) $this->answerId;
    }

}
