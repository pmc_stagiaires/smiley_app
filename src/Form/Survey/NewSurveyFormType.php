<?php


namespace App\Form\Survey;


use App\Entity\Survey;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewSurveyFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('surveyTitle', TextType::class, array('required' => false, 'attr' => array('class' => 'form-control')))
            ->add('surveyDescription', TextType::class, array('required' => false, 'attr' => array('class' => 'form-control')))
            ->add('surveyVersion', TextType::class, array('required' => false, 'attr' => array('class' => 'form-control')))
            ->add('surveyBackgroundPath', FileType::class, ['required' => false, 'label' => 'Image background', 'attr' => array('accept' => 'image/jpeg,image/png' )])
            ->add('save', SubmitType::class, array('label' => 'Create', 'attr' => array('class' => 'btn btn-primary
                mt-3')))->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Survey::class,
        ]);
    }
}