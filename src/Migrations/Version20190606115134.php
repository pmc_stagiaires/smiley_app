<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190606115134 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE User (userId INT AUTO_INCREMENT NOT NULL, email VARCHAR(100) NOT NULL, password VARCHAR(60) NOT NULL, activationCode VARCHAR(50) NOT NULL, deactivated TINYINT(1) DEFAULT NULL, createdDate DATE DEFAULT NULL, deletedDate DATE DEFAULT NULL, modifiedDate DATE DEFAULT NULL, createdBy VARCHAR(100) DEFAULT NULL, modifiedBy VARCHAR(100) DEFAULT NULL, deletedBy VARCHAR(100) DEFAULT NULL, role VARCHAR(50) DEFAULT NULL, PRIMARY KEY(userId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE migrationversions CHANGE version version VARCHAR(14) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Company DROP FOREIGN KEY FK_800230D364B64DCC');
        $this->addSql('DROP TABLE User');
        $this->addSql('ALTER TABLE MigrationVersions CHANGE version version VARCHAR(14) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
