<?php


namespace App\Form\Survey;


use App\Entity\Answer;
use App\Entity\Company;
use App\Entity\Page;
use App\Entity\Question;
use App\Entity\Survey;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class NewAnswerFormType extends AbstractType
{

    private $SurveyQuestionRepository;
    private $Question;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->SurveyQuestionRepository = $entityManager->getRepository(Question::class);
        $this->Question = $entityManager->getRepository(Survey::class);
    }

//    public function buildForm(FormBuilderInterface $builder, array $options)
//    {
//
//    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Answer::class,
        ]);
    }

    private function getQuestionsForSurvey($surveyId)
    {
        return $this->SurveyQuestionRepository->findOneBy(['surveyId' => $surveyId]);
    }
}