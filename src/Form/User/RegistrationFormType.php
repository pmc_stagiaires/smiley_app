<?php

namespace App\Form\User;

use App\Entity\Company;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('companyId', EntityType::class, ['class' => Company::class,
                'choice_label' => function (Company $company) {
                    return sprintf($company->getCompanyName(), $company->getCompanyName());
                },
                'placeholder' => 'Choose an company',
                'label' => 'For which company does the employee work?',
                'attr' => ['class' => 'form-control']])

            ->add('role', ChoiceType::class, array('label' => 'Rol gebruiker',
                'choices' => array(
                    'User' => 'ROLE_USER',
                    'Admin' => 'ROLE_ADMIN',
                    'SuperAdmin' => 'ROLE_SUPER_ADMIN'
                )))
            ->add('email', RepeatedType::class, [
                'type' => EmailType::class,
                'first_options' => array('label' => 'Email adres', 'attr' => [
                    'class' => 'form-control'
                ]),
                'second_options' => array('label' => 'Herhaal email adres', 'attr' => [
                    'class' => 'form-control'
                ])

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }


}
