<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\Device;
use App\Form\Company\EditCAccountFormType;
use App\Form\Company\EditCompanyFormType;
use App\Form\Company\NewCompanyFormType;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @property TokenStorageInterface tokenStorage
 */
class CompanyController extends AbstractController
{
    /**
     * @Route("companies", name="companyPage")
     * @Method({"GET"})
     */
    public function index() {

        $currentUser = $this->getUser();
        $companies = $this->getDoctrine()->getRepository(Company::class)->findAll();
        return $this->render('pages/company.html.twig', [
            'companies' => $companies
        ]);
    }

    public function getCurrentUser()
    {
        $currentUser = $this->getUser();

        if ($currentUser) {
            return $currentUser;
        } else {
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/companies/new", name="newCompanies")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function new(Request $request)
    {
        $Company = new Company();

        $form = $this->createForm(NewCompanyFormType::class, $Company);

        $Company->setCreatedDate(new \DateTime());
        $Company->setModifiedDate(new \DateTime());
        $Company->setCreatedBy($this->getCurrentUser()->getuserId());


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $Company = $form->getData();
            $this->addFlash('success', 'New company has been added');
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($Company);
            $entityManager->flush();

            return $this->redirectToRoute('companyPage');
        }

        return $this->render('pages/CompanyPages/newCompany.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/companies/edit/{companyId}", name="edit_Company")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $companyId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, $companyId)
    {
        $Company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);

        $form = $this->createForm(EditCompanyFormType::class, $Company);

        $Company->setModifiedBy($this->getCurrentUser()->getuserId());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');

            $this->saveToDatabase($Company);

            return $this->redirectToRoute('companyPage');
        }

        return $this->render('pages/CompanyPages/editCompany.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/companies/{companyId}", name="company_show")
     * @param $companyId
     * @return Response
     */
    public function show($companyId)
    {
        $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
        return $this->render('pages/CompanyPages/showCompany.html.twig', [
            'company' => $company
        ]);
    }

//    /**
//     * @Route("/companies/delete/{companyId}")
//     * @param $companyId
//     */
//    public function delete($companyId)
//    {
//
//        $this->addFlash('delete', 'Company has been deleted!');
//        $company = $this->getDoctrine()->getRepository(Company::class)->find($companyId);
//
//        if ($company) {
//            $this->removeFromDatabase($company);
//        }
//
//        $this->redirectToRoute('companyPage');
//    }


    /**
     * @Route("/companies/delete/{companyId}-{deactivated}")
     * @Method({"GET"})
     * @param $companyId
     * @param $deactivated
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivate($companyId, $deactivated) {
        $companyId = $this->getDoctrine()->getRepository(Company::class)->find($companyId);

        if ($companyId->getCompanyId() != $this->getCurrentUser()->getEmail()) {
            if ($deactivated == 1) {
                $companyId->setDeactivated(NULL);
                $companyId->setDeletedBy(NULL);
                $companyId->setDeletedDate(NULL);
            } else {
                $companyId->setDeactivated(1);
                $companyId->setDeletedBy($this->getCurrentUser()->getuserId());
                $companyId->setDeletedDate(new DateTime());
            }

            $this->saveToDatabase($companyId);
            $this->addFlash('delete', 'Company has been deleted');

            $response = new Response();
            $response->send();

            return $this->redirectToRoute('companyPage');

        } else {
            $this->addFlash('error', 'You don\'t currently have permission to deactivate this user.');
            return $this->redirectToRoute('companyPage');
        }
    }


    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        if (!empty($Entity)) {
            $entityManager->persist($Entity);
        }

        $entityManager->flush();
    }

    /**
     * Function to remove an entity to the database
     * @param Entity
     */
    public function removeFromDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($Entity);
        $entityManager->remove($Entity);

        $entityManager->flush();
    }
}