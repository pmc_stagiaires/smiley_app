// function to go to the next survey page when displaying the survey
function gotoPage(nextPageId) {
    const elmnts = document.getElementsByClassName("surveyPage");
    for (let i = 0; i < elmnts.length; i++) {
        elmnts[i].classList.remove("surveyPageActive");
    }

    const elmnt = document.getElementById(nextPageId);
    elmnt.classList.add("surveyPageActive");
}

function setValue(answerId) {
    document.getElementById(answerId).min = 1;
}

var rangeSlider = function(){
    var slider = $('.range-slider'),
        range = $('.range-slider__range'),
        value = $('.range-slider__value');

    slider.each(function(){

        value.each(function(){
            var value = $(this).prev().attr('value');
            $(this).html(value);
        });

        range.on('input', function(){
            $(this).next(value).html(this.value);
        });
    });
};

rangeSlider();


