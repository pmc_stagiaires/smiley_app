<?php

namespace App\Controller;

use App\Entity\AnsweredQuestion;
use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Survey;
use App\Entity\User;
use App\Form\Account\EditEmailFormType;
use App\Form\Account\EditPasswordFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller
 */

class SecurityController extends AbstractController {
    private $passwordEncoder;
    private $session;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SessionInterface $session) {
        $this->passwordEncoder = $passwordEncoder;
        $this->session = $session;
    }

    /**
     * @Route("/", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/manager", name="manager")
     */
    public function manager() {
        $currentUser = $this->getUser();

        if ($currentUser) {
            $this->session->set('session_values', [
                'currentUserEmail' => $currentUser->getEmail()
            ]);
        } else {
            return $this->redirectToRoute('login');
        }

        $companyId = $this->getUser()->getCompanyId();
        $surveys = $this->getDoctrine()->getRepository(Survey::class)->findBy(['companyId' => $companyId]);
        $locations = $this->getDoctrine()->getRepository(Location::class)->findBy(['companyId' => $companyId]);
        $devices = $this->getDoctrine()->getRepository(device::class)->findAll();
        $answeredquestions = $this->getDoctrine()->getRepository(AnsweredQuestion::class)->findAll();


        if ($currentUser->getDeactivated() == true) {
            $this->addFlash('error-login', 'Can\'t login, this account has been deactivated.');
            return $this->redirectToRoute('login');
        } else {
            return $this->render('pages/manager.html.twig', [
                'surveys' => $surveys,
                'locations' => $locations,
                'devices' => $devices,
                'currentUser' => $currentUser,
                'answeredQuestion' => $answeredquestions
            ]);
        }
    }

    /**
     * This route is use for editing your own account
     * @Route("/editAccount", name="editAccount")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAccount(Request $request) {
        // Get current user
        $user = $this->getUser();

        // Get email address of current user
        $currentEmail = $user->getEmail();

        /*
         * Edit Email form
         */
        $editEmailForm = $this->createForm(EditEmailFormType::class, $user);
        $editEmailForm->handleRequest($request);

        if ($editEmailForm->isSubmitted() && $editEmailForm->isValid()) {

            // Get the credentials
            $credentials = [
                'email' => $currentEmail,
                'password' => $editEmailForm->get('currentPassword')->getData(),
            ];

            // Get the new email address from the form
            $newEmail = $editEmailForm->get('email')->getData();

            // validate credentials
            if ($this->checkCredentials($credentials, $user)) {

                // Check if the email address entered is different from the previous one and not empty
                if ($newEmail != $currentEmail && !empty($newEmail)) {

                    // Set the new email address
                    $user->setEmail($newEmail);

                    // Update the entity
                    $this->saveToDatabase($user);

                    // Update email address set in the session
                    $this->session->set('session_values', [
                        'currentUserEmail' => $this->getUser()->getEmail()
                    ]);

                    // Redirect to the manager page
                    return $this->redirectToRoute('manager');
                } else {
                    $this->addFlash('delete', 'Something went wrong');
//                    die('Something went wrong'); // TODO: flash message "Something went wrong" toevoegen
                }
            } else {
                $this->addFlash('delete', 'Invalid credentials');
                //die("Invalid credentials"); // TODO: flash message "Invalid credentials" toevoegen
            }
        }

        /*
         * Edit Password form
         */
        $editPasswordForm = $this->createForm(EditPasswordFormType::class, $user);
        $editPasswordForm->handleRequest($request);

        if ($editPasswordForm->isSubmitted() && $editPasswordForm->isValid()) {

            // Get the credentials
            $credentials = [
                'email' => $currentEmail,
                'password' => $editPasswordForm->get('currentPassword')->getData(),
            ];

            // Get the new password from the form
            $newPassword = $editPasswordForm->get('newPassword')->getData();

            // Validate credentials
            if ($this->checkCredentials($credentials, $user)) {

                // Check if the entered password is not empty
                if (!empty($newPassword)) {

                    // Encode the new password and set it
                    $user->setPassword($this->passwordEncoder->encodePassword($user, $newPassword));

                    // Update the entity
                    $this->saveToDatabase($user);

                    // Make sure the user needs to re-login
                    return $this->redirectToRoute('logout');
                } else {
                    $this->addFlash('delete', 'Password is empty');
                    //die("Password is empty"); // TODO: flash message "alles naar de tering" toevoegen
                }
            } else {
                $this->addFlash('delete', 'Invalid credentials');
               // die("Invalid credentials"); // TODO: flash message Invalid credentials toevoegen
            }
        }

        return $this->render('pages/AccountPages/editAccount.html.twig', [
            'editEmailForm' => $editEmailForm->createView(),
            'editPasswordForm' => $editPasswordForm->createView()
        ]);
    }

    /**
     * @param $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user) {
        return $this->passwordEncoder->isPasswordValid($user, $credentials['password']);
    }

    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($Entity);
        $entityManager->flush();
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {
        session_destroy();

        return $this->redirectToRoute("login");
    }
}
