<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnswerLanguage
 *
 * @ORM\Table(name="answerlanguage")
 * @ORM\Entity
 */
class AnswerLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="languageCode", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $languageCode;

    /**
     * @var int
     *
     * @ORM\Column(name="answerId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $answerId;

    /**
     * @var string
     *
     * @ORM\Column(name="answerTitle", type="string", length=100, nullable=false)
     */
    private $answerTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="answerValue", type="string", length=250, nullable=false)
     */
    private $answerValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @return int
     */
    public function getLanguageCode(): int
    {
        return $this->languageCode;
    }

    /**
     * @param int $languageCode
     * @return AnswerLanguage
     */
    public function setLanguageCode(int $languageCode): AnswerLanguage
    {
        $this->languageCode = $languageCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getAnswerId(): int
    {
        return $this->answerId;
    }

    /**
     * @param int $answerId
     * @return AnswerLanguage
     */
    public function setAnswerId(int $answerId): AnswerLanguage
    {
        $this->answerId = $answerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswerTitle(): string
    {
        return $this->answerTitle;
    }

    /**
     * @param string $answerTitle
     * @return AnswerLanguage
     */
    public function setAnswerTitle(string $answerTitle): AnswerLanguage
    {
        $this->answerTitle = $answerTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswerValue(): string
    {
        return $this->answerValue;
    }

    /**
     * @param string $answerValue
     * @return AnswerLanguage
     */
    public function setAnswerValue(string $answerValue): AnswerLanguage
    {
        $this->answerValue = $answerValue;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return AnswerLanguage
     */
    public function setCreatedDate(\DateTime $createdDate): AnswerLanguage
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return AnswerLanguage
     */
    public function setModifiedDate(\DateTime $modifiedDate): AnswerLanguage
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return AnswerLanguage
     */
    public function setDeletedDate(?\DateTime $deletedDate): AnswerLanguage
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return AnswerLanguage
     */
    public function setCreatedBy(string $createdBy): AnswerLanguage
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return AnswerLanguage
     */
    public function setModifiedBy(string $modifiedBy): AnswerLanguage
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return AnswerLanguage
     */
    public function setDeletedBy(?string $deletedBy): AnswerLanguage
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }
}
