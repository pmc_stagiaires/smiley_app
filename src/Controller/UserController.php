<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\User;
use App\Form\User\ActivationFormType;
use App\Form\User\EditUserFormType;
use App\Form\User\RegistrationFormType;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use DateTime;


/**
 * @property TokenStorageInterface tokenStorage
 */
class UserController extends AbstractController {
    /**
     * @Route("users", name="userPage")
     * @Method("GET")
     */
    public function index() {
        $currentUser = $this->getUser();
        $currentRole = $currentUser->getRole();
        if ($currentRole == 'ROLE_SUPER_ADMIN') {
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        } else {
            $users = $this->getDoctrine()->getRepository(User::class)->findBy(['companyId' => $currentUser->getCompanyId()]);
        }

        return $this->render('pages/user.html.twig', [
            'currentUser' => $currentUser,
            'users' => $users
        ]);
    }

    /**
     * Function to get the current user object
     * @return mixed|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    Public function getCurrentUser()
    {
        $currentUser = $this->getUser();

        return $currentUser;
    }

    /**
     * @Route("/users/new", name="newUser")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response {
        $currentUser = $this->getUser();
        $company = $this->getDoctrine()->getRepository(Company::class)->findOneBy(['companyId' => $currentUser->getCompanyId()]);
        $companySuperUser = $this->getDoctrine()->getRepository(Company::class)->find(1);

        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRole = $form->get('role')->getData();

            if(!$this->roleExists($userRole)){
                $this->addFlash('delete', 'Role does not exist!');
            }

            if ($userRole == 'ROLE_SUPER_ADMIN') {
                $user->setCompanyId($companySuperUser);
            }

            //Generate temporary password
//            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
//            $tempPassword = substr( str_shuffle( $chars ), 0, 8 );
            $tempPassword = "Welkom!";

            // encode temporary password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $tempPassword
                ));
            $user->setactivationCode($unique_id = $this->uuid = Uuid::uuid4());
            $user->setCreatedDate(new \DateTime());
            $user->setCreatedBy($this->getCurrentUser()->getuserId());

            $this->saveToDatabase($user);

            //$url = $this->generateUrl('sendRegistrationMail', array_merge(array('id' => $user->getUserId(), '/', 'activationCode' => $user->getActivationCode())));

            return $this->redirectToRoute('sendRegistrationMail', array('id' => $user->getUserId()));
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/users/{userId}", name="show_User")
     * @param $userId
     * @return object
     */
    public function show($userId) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        return $this->render('pages/UserPages/showUser.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/users/edit/{userId}", name="edit_User")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $userId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function edit(Request $request, $userId){
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        $form = $this->createForm(EditUserFormType::class, $user);

        $user->setModifiedBy($this->getCurrentUser()->getuserId());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', 'Changes have been saved');
            $this->saveToDatabase($user);

            return $this->redirectToRoute('userPage');
        }

        return $this->render('pages/UserPages/editUser.html.twig', [
            'form' => $form-> createView()
        ]);
    }

    /**
     * @Route("/users/delete/{userId}-{deactivated}")
     * @Method({"GET"})
     * @param $userId
     * @param $deactivated
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivate($userId, $deactivated) {
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);

        if ($user->getEmail() != $this->getCurrentUser()->getEmail()) {
            if ($deactivated == 1) {
                $user->setDeactivated(NULL);
                $user->setDeletedBy(NULL);
                $user->setDeletedDate(NULL);
            } else {
                $user->setDeactivated(1);
                $user->setDeletedBy($this->getCurrentUser()->getuserId());
                $user->setDeletedDate(new DateTime());
            }

            $this->saveToDatabase($user);

            $response = new Response();
            $response->send();

            return $this->redirectToRoute('userPage');
        } else {
            $this->addFlash('error', 'You don\'t currently have permission to deactivate this user.');
            return $this->redirectToRoute('userPage');
        }
    }

    /**
     * This route activates a user by the activation code received from the url
     * @Route("/activate/{activationCode}", name="activate")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function activateUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // Get the activation code from the url
        $activationCode = $request->get('activationCode');

        // Get the user by the activation code from the url
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(array('activationCode' => $activationCode));

        // Create the activation form
        $form = $this->createForm(ActivationFormType::class, $user);
        $form->handleRequest($request);

        // Get the email address
        $email = $user->getEmail();

        // Check if the form is submitted and is valid
        if($form->isSubmitted() && $form->isValid()) {

            // Check if the email address is the correct one
            if($user->getEmail() == $email && $user->getActivationCode() == $activationCode){

                // Get the plain password from the form
                $plainPassword = $form->get('password')->getData();

                // encode new plain password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $plainPassword
                    ));

                // Update the database
                $this->saveToDatabase($user);

                $this->addFlash('success', 'Your account has been activated');
                return $this->redirectToRoute('login'); //
            }
            $this->addFlash('delete', 'Something went wrong');
            return $this->redirectToRoute('activate'); //
        }
        return $this->render('pages/UserPages/activateUser.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($Entity);
        $entityManager->flush();
    }

    public function removeFromDatabase($Entity) {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($Entity);
        $entityManager->remove($Entity);

        $entityManager->flush();
    }

    /**
     * Shorten a string to fit and append some text
     * @static
     * @param string $str
     * @param int $length
     * @param string $append
     * @return string
     */
    public static function ShortString($str, $length, $append = "..")
    {
        if (strlen($str) > $length) {
            $str = substr($str, 0, $length - strlen($append)) . $append;
        }
        return $str;
    }

    /**
     * Function to check if the role entered in the form is one of the existing roles
     * @param $role
     * @return bool
     */
    public function roleExists($role){
        $permittedRoles = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER'];

        foreach($permittedRoles as $permittedRole){
            if($permittedRole == $role){
                return true;
                break;
            }
        }
        return false;
    }
}