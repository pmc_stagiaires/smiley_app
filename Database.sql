-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 23, 2019 at 06:45 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_smiley`
--
CREATE DATABASE IF NOT EXISTS `db_smiley` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_smiley`;

-- --------------------------------------------------------

--
-- Table structure for table `answer`
--

CREATE TABLE `answer` (
  `answerId` int(25) NOT NULL,
  `questionId` int(25) NOT NULL,
  `languageCode` int(25) NOT NULL,
  `weighting` int(25) NOT NULL,
  `answerType` varchar(25) NOT NULL,
  `answerValue` varchar(100) DEFAULT NULL,
  `answerNumber` int(25) NOT NULL,
  `answerImagePath` varchar(50) DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `answeredpage`
--

CREATE TABLE `answeredpage` (
  `answeredPageId` int(25) NOT NULL,
  `pageId` int(25) NOT NULL,
  `answeredQuestionId` int(25) NOT NULL,
  `surveyId` int(25) NOT NULL,
  `sessionId` int(25) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `answeredquestion`
--

CREATE TABLE `answeredquestion` (
  `answeredQuestionId` int(25) NOT NULL,
  `questionId` int(25) NOT NULL,
  `answerId` int(25) NOT NULL,
  `sessionId` int(25) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `answerlanguage`
--

CREATE TABLE `answerlanguage` (
  `languageCode` int(25) NOT NULL,
  `answerId` int(25) NOT NULL,
  `answerTitle` varchar(100) NOT NULL,
  `answerValue` varchar(250) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `companyId` int(25) NOT NULL,
  `userId` int(25) NOT NULL,
  `companyName` varchar(100) NOT NULL,
  `createdDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `modifiedDate` date NOT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) DEFAULT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `deviceId` int(25) NOT NULL,
  `locationId` int(25) NOT NULL,
  `deviceLicense` varchar(25) NOT NULL,
  `createdDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `modifiedDate` date NOT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `locationId` int(25) NOT NULL,
  `companyId` int(25) NOT NULL,
  `address` varchar(100) NOT NULL,
  `postalCode` varchar(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `createdDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `modifiedDate` date NOT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `pageId` int(25) NOT NULL,
  `surveyId` int(25) NOT NULL,
  `pageNumber` int(25) NOT NULL,
  `pageTitle` varchar(25) NOT NULL,
  `nextPage` varchar(25) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `questionId` int(25) NOT NULL,
  `surveyId` int(25) NOT NULL,
  `answerId` int(25) NOT NULL,
  `pageId` int(25) NOT NULL,
  `languageCode` int(25) NOT NULL,
  `deactivated` tinyint(1) DEFAULT NULL,
  `questionNumber` int(25) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL,
  `questionTitle` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questionlanguage`
--

CREATE TABLE `questionlanguage` (
  `languageCode` int(25) NOT NULL,
  `questionId` int(25) NOT NULL,
  `questionTitle` varchar(100) NOT NULL,
  `questionDescription` varchar(250) DEFAULT NULL,
  `questionValue` varchar(100) DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `sessionId` int(25) NOT NULL,
  `deviceId` int(25) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `surveyId` int(25) NOT NULL,
  `locationId` int(25) NOT NULL,
  `pageId` int(25) DEFAULT NULL,
  `surveyResultsId` int(25) DEFAULT NULL,
  `surveyTitle` varchar(100) NOT NULL,
  `surveyDescription` varchar(250) NOT NULL,
  `surveyVersion` varchar(25) NOT NULL,
  `surveyBackgroundPath` varchar(50) DEFAULT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modiifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surveyresult`
--

CREATE TABLE `surveyresult` (
  `surveyResultsId` int(25) NOT NULL,
  `deviceId` int(25) NOT NULL,
  `locationId` int(25) NOT NULL,
  `surveyId` int(25) NOT NULL,
  `sessionId` int(25) NOT NULL,
  `createdDate` date NOT NULL,
  `modifiedDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) NOT NULL,
  `deletedBy` varchar(25) DEFAULT NULL,
  `answeredPageId` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userId` int(25) NOT NULL,
  `role` int(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `activationCode` varchar(50) NOT NULL,
  `deactivated` tinyint(1) DEFAULT NULL,
  `createdDate` date NOT NULL,
  `deletedDate` date DEFAULT NULL,
  `modifiedDate` date DEFAULT NULL,
  `createdBy` varchar(25) NOT NULL,
  `modifiedBy` varchar(25) DEFAULT NULL,
  `deletedBy` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userId`, `role`, `email`, `password`, `activationCode`, `deactivated`, `createdDate`, `deletedDate`, `modifiedDate`, `createdBy`, `modifiedBy`, `deletedBy`) VALUES
(1, 1, 'admin@admin.com', 'iamadmin', 'f3c5a3f7-4646-4f9e-9f76-088fff8a1c24', NULL, '2019-05-22', NULL, '2019-05-22', 'User 1', 'User 1', NULL),
(2, 2, 'user@user.com', 'iamuser', 'ba842ce4-9b2c-49be-9d5c-3af365cf2a21', NULL, '2019-05-22', NULL, '2019-05-22', 'User 1', 'User 1', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer`
--
ALTER TABLE `answer`
  ADD PRIMARY KEY (`answerId`),
  ADD KEY `AnswerQuestion` (`questionId`),
  ADD KEY `Language` (`languageCode`);

--
-- Indexes for table `answeredpage`
--
ALTER TABLE `answeredpage`
  ADD PRIMARY KEY (`answeredPageId`),
  ADD KEY `AnswerPagePage` (`pageId`),
  ADD KEY `AnswerPageQuestion` (`answeredQuestionId`),
  ADD KEY `AnswerPageSession` (`sessionId`),
  ADD KEY `AnswerPageSurvey` (`surveyId`);

--
-- Indexes for table `answeredquestion`
--
ALTER TABLE `answeredquestion`
  ADD PRIMARY KEY (`answeredQuestionId`),
  ADD KEY `AnswerQuestionAnswer` (`answerId`),
  ADD KEY `AnswerQuestionQuestion` (`questionId`),
  ADD KEY `AnswerQuestionSession` (`sessionId`);

--
-- Indexes for table `answerlanguage`
--
ALTER TABLE `answerlanguage`
  ADD PRIMARY KEY (`languageCode`,`answerId`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`companyId`),
  ADD KEY `CompanyUser` (`userId`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`deviceId`),
  ADD KEY `DeviceLocation` (`locationId`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`locationId`),
  ADD KEY `CompanyLocation` (`companyId`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`pageId`),
  ADD KEY `PageSurvey` (`surveyId`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`questionId`),
  ADD KEY `QuestionAnswer` (`answerId`),
  ADD KEY `QuestionPage` (`pageId`),
  ADD KEY `QuestionSurvey` (`surveyId`),
  ADD KEY `QuestionLanguage` (`languageCode`);

--
-- Indexes for table `questionlanguage`
--
ALTER TABLE `questionlanguage`
  ADD PRIMARY KEY (`languageCode`,`questionId`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`sessionId`),
  ADD KEY `SessionDevice` (`deviceId`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`surveyId`),
  ADD KEY `SurveyLocation` (`locationId`),
  ADD KEY `SurveyPage` (`pageId`),
  ADD KEY `SurveySurveyResult` (`surveyResultsId`);

--
-- Indexes for table `surveyresult`
--
ALTER TABLE `surveyresult`
  ADD PRIMARY KEY (`surveyResultsId`),
  ADD KEY `SurveyResultDevice` (`deviceId`),
  ADD KEY `SurveyResultLocation` (`locationId`),
  ADD KEY `SurveyResultSession` (`sessionId`),
  ADD KEY `SurveyResultSurvey` (`surveyId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer`
--
ALTER TABLE `answer`
  MODIFY `answerId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `answeredpage`
--
ALTER TABLE `answeredpage`
  MODIFY `answeredPageId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `answeredquestion`
--
ALTER TABLE `answeredquestion`
  MODIFY `answeredQuestionId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `companyId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `device`
--
ALTER TABLE `device`
  MODIFY `deviceId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `locationId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `pageId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `questionId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `sessionId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `surveyId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surveyresult`
--
ALTER TABLE `surveyresult`
  MODIFY `surveyResultsId` int(25) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer`
--
ALTER TABLE `answer`
  ADD CONSTRAINT `AnswerQuestion` FOREIGN KEY (`questionId`) REFERENCES `question` (`questionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Language` FOREIGN KEY (`languageCode`) REFERENCES `answerlanguage` (`languageCode`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `answeredpage`
--
ALTER TABLE `answeredpage`
  ADD CONSTRAINT `AnswerPagePage` FOREIGN KEY (`pageId`) REFERENCES `page` (`pageId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AnswerPageQuestion` FOREIGN KEY (`answeredQuestionId`) REFERENCES `answeredquestion` (`answeredQuestionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AnswerPageSession` FOREIGN KEY (`sessionId`) REFERENCES `session` (`sessionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AnswerPageSurvey` FOREIGN KEY (`surveyId`) REFERENCES `survey` (`surveyId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `answeredquestion`
--
ALTER TABLE `answeredquestion`
  ADD CONSTRAINT `AnswerQuestionAnswer` FOREIGN KEY (`answerId`) REFERENCES `answer` (`answerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AnswerQuestionQuestion` FOREIGN KEY (`questionId`) REFERENCES `question` (`questionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AnswerQuestionSession` FOREIGN KEY (`sessionId`) REFERENCES `session` (`sessionId`);

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `CompanyUser` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `device`
--
ALTER TABLE `device`
  ADD CONSTRAINT `DeviceLocation` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `location`
--
ALTER TABLE `location`
  ADD CONSTRAINT `CompanyLocation` FOREIGN KEY (`companyId`) REFERENCES `company` (`companyId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `PageSurvey` FOREIGN KEY (`surveyId`) REFERENCES `survey` (`surveyId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `QuestionAnswer` FOREIGN KEY (`answerId`) REFERENCES `answer` (`answerId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `QuestionLanguage` FOREIGN KEY (`languageCode`) REFERENCES `questionlanguage` (`languageCode`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `QuestionPage` FOREIGN KEY (`pageId`) REFERENCES `page` (`pageId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `QuestionSurvey` FOREIGN KEY (`surveyId`) REFERENCES `survey` (`surveyId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `SessionDevice` FOREIGN KEY (`deviceId`) REFERENCES `device` (`deviceId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `survey`
--
ALTER TABLE `survey`
  ADD CONSTRAINT `SurveyLocation` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SurveyPage` FOREIGN KEY (`pageId`) REFERENCES `page` (`pageId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SurveySurveyResult` FOREIGN KEY (`surveyResultsId`) REFERENCES `surveyresult` (`surveyResultsId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `surveyresult`
--
ALTER TABLE `surveyresult`
  ADD CONSTRAINT `SurveyResultDevice` FOREIGN KEY (`deviceId`) REFERENCES `device` (`deviceId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SurveyResultLocation` FOREIGN KEY (`locationId`) REFERENCES `location` (`locationId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SurveyResultSession` FOREIGN KEY (`sessionId`) REFERENCES `session` (`sessionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `SurveyResultSurvey` FOREIGN KEY (`surveyId`) REFERENCES `survey` (`surveyId`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
