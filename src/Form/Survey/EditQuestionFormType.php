<?php

namespace App\Form\Survey;

use App\Entity\Page;
use App\Entity\Question;
use App\Entity\Survey;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditQuestionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('questionNumber', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('questionTitle', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('weighting', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('answerTypes', ChoiceType::class, array(
                'required' => true, 'attr' => [
                    'class' => 'form-control',
                    'style' => 'margin:5px 0;'
                ],
                'choices' => [
                    'MultipleChoice' => 'MultipleChoice',
                    'OpenQuestion' => 'OpenQuestion',
                    'ImagePath' => 'ImagePath',
                    'Smiley' => 'Smiley',
                    'Scale' => 'Scale',
                    'Closed' => 'Closed',
                    'NetPromoterScore' => 'NetPromoterScore'
                ]
            ))
            ->add('pageId', EntityType::class, [
                'required' => true, 'attr' => [
                    'id' => 'sel1',
                    'class' => 'form-control'
                ], 'class' => Page::class, 'choice_label' => function (Page $Page) {
                    return sprintf($Page->getPageTitle());
                }
            ])
            ->add('surveyId', EntityType::class, [
                'required' => true, 'attr' => [
                    'id' => 'sel1',
                    'class' => 'form-control'
                ], 'class' => Survey::class, 'choice_label' => function (Survey $Survey) {
                    return sprintf($Survey->getSurveyTitle());
                }
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Submit', 'attr' => [
                    'class' => 'btn btn-primary mt-3'
                ]])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}