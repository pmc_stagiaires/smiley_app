<?php


namespace App\Controller;

use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Company;
use App\Form\Location\EditLocationFormType;
use App\Form\Location\NewLocationFormType;
use DateTime;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @property TokenStorageInterface tokenStorage
 */
class LocationController extends AbstractController
{
    /**
     * @Route("locations", name="locationPage")
     * @Method({"GET"})
     */
    public function index() {
        $currentUser = $this->getUser();
        $currentRole = $currentUser->getRole();
        if ($currentRole == 'ROLE_SUPER_ADMIN') {
            $locations = $this->getDoctrine()->getRepository(Location::class)->findAll();
            $devices = $this->getDoctrine()->getRepository(Device::class)->findBy(array('locationId' => $locations));
        } else {
            $locations = $this->getDoctrine()->getRepository(Location::class)->findBy(array('companyId' => $currentUser->getCompanyId()));
            $devices = $this->getDoctrine()->getRepository(Device::class)->findBy(array('locationId' => $locations));
        }

        return $this->render('pages/location.html.twig', [
            'locations' => $locations,
            'devices' => $devices
        ]);
    }


    public function getCurrentUser()
    {
        $currentUser = $this->getUser();

        if($currentUser){
            return $currentUser;
        } else {
            return $this->redirectToRoute('login');
        }
    }

    /**
     * @Route("/locations/new", name="newLocations")
     * @Method({ "GET", "POST"})
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function new(Request $request) {
        $Location = new Location();

        $form = $this->createForm(NewLocationFormType::class, $Location);

        $Location->setCreatedDate(new \DateTime());
        $Location->setModifiedDate(new \DateTime());
        $Location->setCreatedBy($this->getCurrentUser()->getuserId());
        $Location->setModifiedBy($this->getCurrentUser()->getuserId());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $Location = $form->getData();
            $this->addFlash('success', 'New location has been added');
            $this->saveToDatabase($Location);

            return $this->redirectToRoute('locationPage');
        }

        return $this->render('pages/LocationPages/newLocation.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/locations/edit/{LocationId}", name="editLocation")
     * Method({ "GET", "POST"})
     * @param Request $request
     * @param $LocationId
     * @return RedirectResponse|Response
     */
    public function edit(Request $request, $LocationId) {
        $location = $this->getDoctrine()->getRepository(Location::class)->find($LocationId);

        $currentUser = $this->getUser();
        $currentRole = $currentUser->getRole();
        if ($currentRole == 'ROLE_SUPER_ADMIN') {
            $form = $this->createForm(EditLocationFormType::class, $location);
        }
        else{
            $form = $this->createForm(EditLocationFormType::class, $location);
        }




        $location->setModifiedDate(new \DateTime());
        $location->setModifiedBy($this->getCurrentUser()->getuserId());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->addFlash('success', 'Changes have been saved');
            $this->saveToDatabase($location);

            return $this->redirectToRoute('locationPage');
        }

        return $this->render('pages/LocationPages/editLocation.html.twig', [
            'form' => $form-> createView()
        ]);
    }

    /**
     * @Route("/locations/{LocationId}", name="LocationShow")
     * @param $LocationId
     * @return Response
     */
    public function show($LocationId) {
        $location = $this->getDoctrine()->getRepository(Location::class)->find($LocationId);

//      TODO fix this
        $currentUser = $this->getUser();
        $currentRole = $currentUser->getRole();
        if ($currentRole == 'ROLE_SUPER_ADMIN') {
            return $this->render('pages/LocationPages/showLocation.html.twig', [
                'Location' => $location
            ]);
        }
        else{
            return $this->render('pages/LocationPages/showLocation.html.twig', [
                'Location' => $location
            ]);
        }

    }

//    /**
//     * @Route("/locations/delete/{LocationId}")
//     * @Method({"DELETE"})
//     * @param $LocationId
//     * @return RedirectResponse
//     */
//    public function delete($LocationId) {
//        $location = $this->getDoctrine()->getRepository(Location::class)->find($LocationId);
//
//        $currentUser = $this->getUser();
//        $currentRole = $currentUser->getRole();
//        if ($currentRole == 'ROLE_SUPER_ADMIN') {
//            $this->removeFromDatabase($location);
//        } elseif ($location->getCompanyId() == $this->getUser()->getCompanyId()) {
//            $this->addFlash('error', 'You don\'t currently have permission to this location.');
//            return $this->redirectToRoute('locationPage');
//        } else{
//            $this->removeFromDatabase($location);
//        }
//
//
//        $this->addFlash('delete', 'Location has been deleted!');
//        $response = new Response();
//        $response->send();
//    }

    /**
     * @Route("/locations/delete/{locationId}-{deactivated}")
     * @Method({"GET"})
     * @param $locationId
     * @param $deactivated
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deactivate($locationId, $deactivated) {
        $locationId = $this->getDoctrine()->getRepository(Location::class)->find($locationId);

        if ($locationId->getLocationId() != $this->getCurrentUser()->getEmail()) {
            if ($deactivated == 1) {
                $locationId->setDeactivated(NULL);
                $locationId->setDeletedBy(NULL);
                $locationId->setDeletedDate(NULL);
            } else {
                $locationId->setDeactivated(1);
                $locationId->setDeletedBy($this->getCurrentUser()->getuserId());
                $locationId->setDeletedDate(new DateTime());
            }

            $this->saveToDatabase($locationId);
            $this->addFlash('delete', 'Location has been deleted');

            $response = new Response();
            $response->send();

            return $this->redirectToRoute('locationPage');

        } else {
            $this->addFlash('error', 'You don\'t currently have permission to deactivate this user.');
            return $this->redirectToRoute('locationPage');
        }
    }

    /**
     * Function to save an entity to the database
     * @param Entity
     */
    public function saveToDatabase($Entity)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($Entity);
        $entityManager->flush();
    }

    public function removeFromDatabase($Entity) {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($Entity);
        $entityManager->remove($Entity);

        $entityManager->flush();
    }
}