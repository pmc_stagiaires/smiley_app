<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 *
 * @ORM\Table(name="page", indexes={@ORM\Index(name="pageSurvey", columns={"surveyId"})})
 * @ORM\Entity
 */
class Page {
    /**
     * @var int
     *
     * @ORM\Column(name="pageId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pageId;

    /**
     * @var int
     *
     * @ORM\Column(name="pageNumber", type="integer", nullable=false)
     */
    private $pageNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="pageTitle", type="string", length=25, nullable=false)
     */
    private $pageTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="previousPage", type="string", length=25, nullable=false)
     */
    private $previousPage;

    /**
     * @var string
     *
     * @ORM\Column(name="nextPage", type="string", length=25, nullable=false)
     */
    private $nextPage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @return int
     */
    public function getPageId(): int
    {
        return $this->pageId;
    }

    /**
     * @param int $pageId
     * @return Page
     */
    public function setPageId(int $pageId)
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     *
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     * @return Page
     */
    public function setPageNumber(int $pageNumber): Page
    {
        $this->pageNumber = $pageNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     * @return Page
     */
    public function setPageTitle(string $pageTitle): Page
    {
        $this->pageTitle = $pageTitle;
        return $this;
    }

    /**
* @return string
*/
    public function getPreviousPage()
    {
        return $this->previousPage;
    }

    /**
     * @param string $previousPage
     * @return Page
     */
    public function setPreviousPage($previousPage)
    {
        $this->previousPage = $previousPage;
        return $this;
    }

    /**
     * @return string
     */
    public function getNextPage()
    {
        return $this->nextPage;
    }

    /**
     * @param string $nextPage
     * @return Page
     */
    public function setNextPage(string $nextPage): Page
    {
        $this->nextPage = $nextPage;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate(): \DateTime
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return Page
     */
    public function setCreatedDate(\DateTime $createdDate): Page
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate(): \DateTime
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return Page
     */
    public function setModifiedDate(\DateTime $modifiedDate): Page
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate(): ?\DateTime
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return Page
     */
    public function setDeletedDate(?\DateTime $deletedDate): Page
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy(): string
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Page
     */
    public function setCreatedBy(string $createdBy): Page
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy(): string
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return Page
     */
    public function setModifiedBy(string $modifiedBy): Page
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return Page
     */
    public function setDeletedBy(?string $deletedBy): Page
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return \Survey
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param $surveyId
     * @return Page
     */
    public function setSurveyId($surveyId)
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    public function __toInt()
    {
        return $this->pageId;
    }
}
