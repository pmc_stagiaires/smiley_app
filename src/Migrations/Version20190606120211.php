<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190606120211 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Answer (answerId INT AUTO_INCREMENT NOT NULL, weighting INT NOT NULL, answerType VARCHAR(25) NOT NULL, answerValue VARCHAR(100) DEFAULT NULL, answerNumber INT NOT NULL, answerImagePath VARCHAR(150) DEFAULT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, questionId INT DEFAULT NULL, languageCode INT DEFAULT NULL, INDEX Language (languageCode), INDEX AnswerQuestion (questionId), PRIMARY KEY(answerId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE AnsweredPage (answeredPageIds INT AUTO_INCREMENT NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, pageId INT DEFAULT NULL, answeredQuestionId INT DEFAULT NULL, sessionId INT DEFAULT NULL, surveyId INT DEFAULT NULL, INDEX AnswerPageQuestion (answeredQuestionId), INDEX AnswerPageSession (sessionId), INDEX AnswerPagePage (pageId), INDEX AnswerPageSurvey (surveyId), PRIMARY KEY(answeredPageId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE AnsweredQuestion (answeredQuestionId INT AUTO_INCREMENT NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, answerId INT DEFAULT NULL, questionId INT DEFAULT NULL, sessionId INT DEFAULT NULL, INDEX AnsweredQuestionQuestion (questionId), INDEX AnsweredQuestionSession (sessionId), INDEX AnsweredQuestionAnswer (answerId), PRIMARY KEY(answeredQuestionId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE AnswerLanguage (languageCode INT NOT NULL, answerId INT NOT NULL, answerTitle VARCHAR(100) NOT NULL, answerValue VARCHAR(250) NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, PRIMARY KEY(languageCode, answerId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Company (companyId INT AUTO_INCREMENT NOT NULL, companyName VARCHAR(100) NOT NULL, createdDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, modifiedDate DATE NOT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) DEFAULT NULL, deletedBy VARCHAR(100) DEFAULT NULL, userId INT DEFAULT NULL, INDEX CompanyUser (userId), PRIMARY KEY(companyId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Device (deviceId INT AUTO_INCREMENT NOT NULL, deviceLicense VARCHAR(25) NOT NULL, createdDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, modifiedDate DATE NOT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, locationId INT DEFAULT NULL, INDEX DeviceLocation (locationId), PRIMARY KEY(deviceId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Location (locationId INT AUTO_INCREMENT NOT NULL, address VARCHAR(100) NOT NULL, postalCode VARCHAR(10) NOT NULL, city VARCHAR(50) NOT NULL, createdDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, modifiedDate DATE NOT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, companyId INT DEFAULT NULL, INDEX CompanyLocation (companyId), PRIMARY KEY(locationId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE MigrationVersions (version VARCHAR(14) NOT NULL, executedAt DATE NOT NULL, PRIMARY KEY(version)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Page (pageId INT AUTO_INCREMENT NOT NULL, pageNumber INT NOT NULL, pageTitle VARCHAR(25) NOT NULL, nextPage VARCHAR(25) NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, surveyId INT DEFAULT NULL, INDEX pageSurvey (surveyId), PRIMARY KEY(pageId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Question (questionId INT AUTO_INCREMENT NOT NULL, deactivated TINYINT(1) DEFAULT NULL, questionNumber INT NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, questionTitle VARCHAR(25) NOT NULL, answerId INT DEFAULT NULL, languageCode INT DEFAULT NULL, pageId INT DEFAULT NULL, surveyId INT DEFAULT NULL, INDEX QuestionPage (pageId), INDEX QuestionSurvey (surveyId), INDEX QuestionAnswer (answerId), INDEX QuestionLanguage (languageCode), PRIMARY KEY(questionId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE QuestionLanguage (languageCode INT NOT NULL, questionId INT NOT NULL, questionTitle VARCHAR(100) NOT NULL, questionDescription VARCHAR(250) DEFAULT NULL, questionValue VARCHAR(100) DEFAULT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, PRIMARY KEY(languageCode, questionId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Session (sessionId INT AUTO_INCREMENT NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, deviceId INT DEFAULT NULL, INDEX SessionDevice (deviceId), PRIMARY KEY(sessionId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Survey (surveyId INT AUTO_INCREMENT NOT NULL, surveyTitle VARCHAR(100) NOT NULL, surveyDescription VARCHAR(250) NOT NULL, surveyVersion VARCHAR(25) NOT NULL, surveyBackgroundPath VARCHAR(50) DEFAULT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, locationId INT DEFAULT NULL, pageId INT DEFAULT NULL, surveyResultsId INT DEFAULT NULL, INDEX SurveyPage (pageId), INDEX SurveyResult (surveyResultsId), INDEX SurveyLocation (locationId), PRIMARY KEY(surveyId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE SurveyResult (surveyResultsId INT AUTO_INCREMENT NOT NULL, createdDate DATE NOT NULL, modifiedDate DATE NOT NULL, deletedDate DATE DEFAULT NULL, createdBy VARCHAR(100) NOT NULL, modifiedBy VARCHAR(100) NOT NULL, deletedBy VARCHAR(100) DEFAULT NULL, answeredPageId INT NOT NULL, deviceId INT DEFAULT NULL, locationId INT DEFAULT NULL, sessionId INT DEFAULT NULL, surveyId INT DEFAULT NULL, INDEX surveyResultLocation (locationId), INDEX surveyResultSession (sessionId), INDEX surveyResultDevice (deviceId), INDEX surveyResultSurvey (surveyId), PRIMARY KEY(surveyResultsId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE User (userId INT AUTO_INCREMENT NOT NULL, email VARCHAR(100) NOT NULL, password VARCHAR(60) NOT NULL, activationCode VARCHAR(50) NOT NULL, deactivated TINYINT(1) DEFAULT NULL, createdDate DATE DEFAULT NULL, deletedDate DATE DEFAULT NULL, modifiedDate DATE DEFAULT NULL, createdBy VARCHAR(100) DEFAULT NULL, modifiedBy VARCHAR(100) DEFAULT NULL, deletedBy VARCHAR(100) DEFAULT NULL, role VARCHAR(50) DEFAULT NULL, PRIMARY KEY(userId)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Answer ADD CONSTRAINT FK_DD714F134B476EBA FOREIGN KEY (questionId) REFERENCES Question (questionId)');
        $this->addSql('ALTER TABLE Answer ADD CONSTRAINT FK_DD714F134BCA2970 FOREIGN KEY (languageCode) REFERENCES AnswerLanguage (languageCode)');
        $this->addSql('ALTER TABLE AnsweredPage ADD CONSTRAINT FK_1906F6F29D3B65E3 FOREIGN KEY (pageId) REFERENCES Page (pageId)');
        $this->addSql('ALTER TABLE AnsweredPage ADD CONSTRAINT FK_1906F6F2530A1F0C FOREIGN KEY (answeredQuestionId) REFERENCES AnsweredQuestion (answeredQuestionId)');
        $this->addSql('ALTER TABLE AnsweredPage ADD CONSTRAINT FK_1906F6F23950B5F6 FOREIGN KEY (sessionId) REFERENCES Session (sessionId)');
        $this->addSql('ALTER TABLE AnsweredPage ADD CONSTRAINT FK_1906F6F2F5690AD0 FOREIGN KEY (surveyId) REFERENCES Survey (surveyId)');
        $this->addSql('ALTER TABLE AnsweredQuestion ADD CONSTRAINT FK_9834862D5447E146 FOREIGN KEY (answerId) REFERENCES Answer (answerId)');
        $this->addSql('ALTER TABLE AnsweredQuestion ADD CONSTRAINT FK_9834862D4B476EBA FOREIGN KEY (questionId) REFERENCES Question (questionId)');
        $this->addSql('ALTER TABLE AnsweredQuestion ADD CONSTRAINT FK_9834862D3950B5F6 FOREIGN KEY (sessionId) REFERENCES Session (sessionId)');
        $this->addSql('ALTER TABLE Company ADD CONSTRAINT FK_800230D364B64DCC FOREIGN KEY (userId) REFERENCES User (userId)');
        $this->addSql('ALTER TABLE Device ADD CONSTRAINT FK_E83B3B896D7286D FOREIGN KEY (locationId) REFERENCES Location (locationId)');
        $this->addSql('ALTER TABLE Location ADD CONSTRAINT FK_A7E8EB9D2480E723 FOREIGN KEY (companyId) REFERENCES Company (companyId)');
        $this->addSql('ALTER TABLE Page ADD CONSTRAINT FK_B438191EF5690AD0 FOREIGN KEY (surveyId) REFERENCES Survey (surveyId)');
        $this->addSql('ALTER TABLE Question ADD CONSTRAINT FK_4F812B185447E146 FOREIGN KEY (answerId) REFERENCES Answer (answerId)');
        $this->addSql('ALTER TABLE Question ADD CONSTRAINT FK_4F812B184BCA2970 FOREIGN KEY (languageCode) REFERENCES QuestionLanguage (languageCode)');
        $this->addSql('ALTER TABLE Question ADD CONSTRAINT FK_4F812B189D3B65E3 FOREIGN KEY (pageId) REFERENCES Page (pageId)');
        $this->addSql('ALTER TABLE Question ADD CONSTRAINT FK_4F812B18F5690AD0 FOREIGN KEY (surveyId) REFERENCES Survey (surveyId)');
        $this->addSql('ALTER TABLE Session ADD CONSTRAINT FK_1FF9EC48ADBFE9A1 FOREIGN KEY (deviceId) REFERENCES Device (deviceId)');
        $this->addSql('ALTER TABLE Survey ADD CONSTRAINT FK_AAF39ECA96D7286D FOREIGN KEY (locationId) REFERENCES Location (locationId)');
        $this->addSql('ALTER TABLE Survey ADD CONSTRAINT FK_AAF39ECA9D3B65E3 FOREIGN KEY (pageId) REFERENCES Page (pageId)');
        $this->addSql('ALTER TABLE Survey ADD CONSTRAINT FK_AAF39ECA53B2AC81 FOREIGN KEY (surveyResultsId) REFERENCES SurveyResult (surveyResultsId)');
        $this->addSql('ALTER TABLE SurveyResult ADD CONSTRAINT FK_8ABF0179ADBFE9A1 FOREIGN KEY (deviceId) REFERENCES Device (deviceId)');
        $this->addSql('ALTER TABLE SurveyResult ADD CONSTRAINT FK_8ABF017996D7286D FOREIGN KEY (locationId) REFERENCES Location (locationId)');
        $this->addSql('ALTER TABLE SurveyResult ADD CONSTRAINT FK_8ABF01793950B5F6 FOREIGN KEY (sessionId) REFERENCES Session (sessionId)');
        $this->addSql('ALTER TABLE SurveyResult ADD CONSTRAINT FK_8ABF0179F5690AD0 FOREIGN KEY (surveyId) REFERENCES Survey (surveyId)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE AnsweredQuestion DROP FOREIGN KEY FK_9834862D5447E146');
        $this->addSql('ALTER TABLE Question DROP FOREIGN KEY FK_4F812B185447E146');
        $this->addSql('ALTER TABLE AnsweredPage DROP FOREIGN KEY FK_1906F6F2530A1F0C');
        $this->addSql('ALTER TABLE Answer DROP FOREIGN KEY FK_DD714F134BCA2970');
        $this->addSql('ALTER TABLE Location DROP FOREIGN KEY FK_A7E8EB9D2480E723');
        $this->addSql('ALTER TABLE Session DROP FOREIGN KEY FK_1FF9EC48ADBFE9A1');
        $this->addSql('ALTER TABLE SurveyResult DROP FOREIGN KEY FK_8ABF0179ADBFE9A1');
        $this->addSql('ALTER TABLE Device DROP FOREIGN KEY FK_E83B3B896D7286D');
        $this->addSql('ALTER TABLE Survey DROP FOREIGN KEY FK_AAF39ECA96D7286D');
        $this->addSql('ALTER TABLE SurveyResult DROP FOREIGN KEY FK_8ABF017996D7286D');
        $this->addSql('ALTER TABLE AnsweredPage DROP FOREIGN KEY FK_1906F6F29D3B65E3');
        $this->addSql('ALTER TABLE Question DROP FOREIGN KEY FK_4F812B189D3B65E3');
        $this->addSql('ALTER TABLE Survey DROP FOREIGN KEY FK_AAF39ECA9D3B65E3');
        $this->addSql('ALTER TABLE Answer DROP FOREIGN KEY FK_DD714F134B476EBA');
        $this->addSql('ALTER TABLE AnsweredQuestion DROP FOREIGN KEY FK_9834862D4B476EBA');
        $this->addSql('ALTER TABLE Question DROP FOREIGN KEY FK_4F812B184BCA2970');
        $this->addSql('ALTER TABLE AnsweredPage DROP FOREIGN KEY FK_1906F6F23950B5F6');
        $this->addSql('ALTER TABLE AnsweredQuestion DROP FOREIGN KEY FK_9834862D3950B5F6');
        $this->addSql('ALTER TABLE SurveyResult DROP FOREIGN KEY FK_8ABF01793950B5F6');
        $this->addSql('ALTER TABLE AnsweredPage DROP FOREIGN KEY FK_1906F6F2F5690AD0');
        $this->addSql('ALTER TABLE Page DROP FOREIGN KEY FK_B438191EF5690AD0');
        $this->addSql('ALTER TABLE Question DROP FOREIGN KEY FK_4F812B18F5690AD0');
        $this->addSql('ALTER TABLE SurveyResult DROP FOREIGN KEY FK_8ABF0179F5690AD0');
        $this->addSql('ALTER TABLE Survey DROP FOREIGN KEY FK_AAF39ECA53B2AC81');
        $this->addSql('ALTER TABLE Company DROP FOREIGN KEY FK_800230D364B64DCC');
        $this->addSql('DROP TABLE Answer');
        $this->addSql('DROP TABLE AnsweredPage');
        $this->addSql('DROP TABLE AnsweredQuestion');
        $this->addSql('DROP TABLE AnswerLanguage');
        $this->addSql('DROP TABLE Company');
        $this->addSql('DROP TABLE Device');
        $this->addSql('DROP TABLE Location');
        $this->addSql('DROP TABLE MigrationVersions');
        $this->addSql('DROP TABLE Page');
        $this->addSql('DROP TABLE Question');
        $this->addSql('DROP TABLE QuestionLanguage');
        $this->addSql('DROP TABLE Session');
        $this->addSql('DROP TABLE Survey');
        $this->addSql('DROP TABLE SurveyResult');
        $this->addSql('DROP TABLE User');
    }
}
