<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190606122426 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE MigrationVersions CHANGE version version VARCHAR(14) NOT NULL');
        $this->addSql('ALTER TABLE user ADD role VARCHAR(50) DEFAULT NULL, DROP salt, DROP roles, CHANGE createdDate createdDate DATE DEFAULT NULL, CHANGE createdBy createdBy VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE MigrationVersions CHANGE version version VARCHAR(14) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE User ADD salt VARCHAR(16) NOT NULL COLLATE utf8mb4_unicode_ci, ADD roles JSON NOT NULL, DROP role, CHANGE createdDate createdDate DATE NOT NULL, CHANGE createdBy createdBy VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci');
    }
}
