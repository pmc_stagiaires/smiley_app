<?php


namespace App\Form\Survey;


use App\Entity\Location;
use App\Entity\Page;
use App\Entity\Question;
use App\Entity\Answer;
use Doctrine\DBAL\Types\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewQuestionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('questionTitle', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control',
                    'autocomplete' => 'off'
                ]
            ])
            ->add('weighting', TextType::class, [
                'required' => true, 'attr' => [
                    'class' => 'form-control'
                ]
            ])
//            ->add('answerType', EntityType::class, ['class' => Answer::class,
//                'choice_label' => function(Answer $answer) {
//                    return sprintf( $answer->getAnswerType(), $answer->getAnswerType());
//                },
//                'placeholder' => 'Choose an question type',
//                'label' => 'Question type',
//                'attr' => ['class' => 'form-control']
//            ])
//            ->add('answerTypes', TextType::class, [
//                'required' => true, 'attr' => [
//                    'class' => 'form-control',
//                    'autocomplete' => 'off'
//                ]
//            ])
            ->add('answerTypes', ChoiceType::class, array(
                'required' => true, 'attr' => [
                    'class' => 'form-control',
                    'style' => 'margin:5px 0;'
                ],
                'choices' => [
                    'MultipleChoice' => 'MultipleChoice',
                    'OpenQuestion' => 'OpenQuestion',
                    'ImagePath' => 'ImagePath',
                    'Smiley' => 'Smiley',
                    'Scale' => 'Scale',
                    'Closed' => 'Closed',
                    'NetPromoterScore' => 'NetPromoterScore'
                ]
            ))
            ->add('save', SubmitType::class, [
                'label' => 'Submit', 'attr' => [
                    'class' => 'btn btn-primary mt-3'
                ]])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
        ]);
    }
}