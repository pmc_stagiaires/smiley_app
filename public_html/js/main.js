// DELETE function
function del(dataId, path) {
    if (confirm('You sure you want to delete this')){
        fetch(`${path}/delete/${dataId}`, { method: 'DELETE'}).then(
            res => window.location.reload()
        );
    }
}

// Displays the Panel name (top title)
window.onload=function changePanelTitle() {
    const elementManager = document.getElementById("managerId");
    if (elementManager) {
        elementManager.innerHTML =  document.title
    }
};

function changeScaleInput(formScaleId, scaleViewId) {
    const scaleElement = document.getElementById(formScaleId);
    const scaleView = document.getElementById(scaleViewId);
    scaleView.innerText = scaleElement.value;
}


$(document).ready(function() {
    $(window).on('load', function(){
        $('.amount-of-devices, .amount-of-surveys').append('<div style="" class="loadingDiv">' +
            '<div class="sk-folding-cube">\n' +
                '<div class="sk-cube1 sk-cube"></div>\n' +
                '<div class="sk-cube2 sk-cube"></div>\n' +
                '<div class="sk-cube4 sk-cube"></div>\n' +
                '<div class="sk-cube3 sk-cube"></div>\n' +
            '</div></div>');
        setTimeout(removeLoader, 2000);
    });
    function removeLoader(){
        $( ".loadingDiv" ).fadeOut(300, function() {
            // fadeOut complete. Remove the loading div
            $( ".loadingDiv" ).remove(); //makes page more lightweight
        });
    }

    $('#dataTable').dataTable();
    $('.dataTables_length').addClass('bs-select');

});

