<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnsweredPage
 *
 * @ORM\Table(name="answeredpage", indexes={@ORM\Index(name="AnswerPageQuestion", columns={"answeredQuestionId"}), @ORM\Index(name="AnswerPageSession", columns={"sessionId"}), @ORM\Index(name="AnswerPagePage", columns={"pageId"}), @ORM\Index(name="AnswerPageSurvey", columns={"surveyId"})})
 * @ORM\Entity
 */
class AnsweredPage {
    /**
     * @var int
     *
     * @ORM\Column(name="answeredPageId", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $answeredPageId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=false)
     */
    private $createdDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=false)
     */
    private $modifiedDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="deletedDate", type="date", nullable=true)
     */
    private $deletedDate;

    /**
     * @var string
     *
     * @ORM\Column(name="createdBy", type="string", length=100, nullable=false)
     */
    private $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=100, nullable=false)
     */
    private $modifiedBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="deletedBy", type="string", length=100, nullable=true)
     */
    private $deletedBy;

    /**
     * @var \Page
     *
     * @ORM\ManyToOne(targetEntity="Page")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pageId", referencedColumnName="pageId")
     * })
     */
    private $pageId;

    /**
     * @var \AnsweredQuestion
     *
     * @ORM\ManyToOne(targetEntity="AnsweredQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="answeredQuestionId", referencedColumnName="answeredQuestionId")
     * })
     */
    private $answeredQuestionId;

    /**
     * @var \Session
     *
     * @ORM\ManyToOne(targetEntity="Session")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sessionId", referencedColumnName="sessionId")
     * })
     */
    private $sessionId;

    /**
     * @var \Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="surveyId", referencedColumnName="surveyId")
     * })
     */
    private $surveyId;

    /**
     * @return int
     */
    public function getAnsweredPageId(): int
    {
        return $this->answeredPageId;
    }

    /**
     * @param int $answeredPageId
     * @return AnsweredPage
     */
    public function setAnsweredPageId(int $answeredPageId)
    {
        $this->answeredPageId = $answeredPageId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     * @return AnsweredPage
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * @param \DateTime $modifiedDate
     * @return AnsweredPage
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDeletedDate()
    {
        return $this->deletedDate;
    }

    /**
     * @param \DateTime|null $deletedDate
     * @return AnsweredPage
     */
    public function setDeletedDate($deletedDate)
    {
        $this->deletedDate = $deletedDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return AnsweredPage
     */
    public function setCreatedBy(string $createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * @param string $modifiedBy
     * @return AnsweredPage
     */
    public function setModifiedBy(string $modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeletedBy(): ?string
    {
        return $this->deletedBy;
    }

    /**
     * @param null|string $deletedBy
     * @return AnsweredPage
     */
    public function setDeletedBy(?string $deletedBy)
    {
        $this->deletedBy = $deletedBy;
        return $this;
    }

    /**
     * @return
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param $pageId
     * @return AnsweredPage
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     * @return \AnsweredQuestion
     */
    public function getAnsweredQuestionId()
    {
        return $this->answeredQuestionId;
    }

    /**
     * @param $answeredQuestionId
     * @return AnsweredPage
     */
    public function setAnsweredQuestionId($answeredQuestionId)
    {
        $this->answeredQuestionId = $answeredQuestionId;
        return $this;
    }

    /**
     * @return \Session
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param \Session $sessionId
     * @return AnsweredPage
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    /**
     * @return
     */
    public function getSurveyId()
    {
        return $this->surveyId;
    }

    /**
     * @param $surveyId
     * @return AnsweredPage
     */
    public function setSurveyId($surveyId)
    {
        $this->surveyId = $surveyId;
        return $this;
    }

    public function __toInt()
    {
        return $this->pageId;
    }

    public function __toInt2(){
        return $this->surveyId;
    }
}
