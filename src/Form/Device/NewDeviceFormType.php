<?php


namespace App\Form\Device;

use App\Entity\Device;
use App\Entity\Location;
use App\Entity\Question;
use App\Entity\Survey;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use function PHPSTORM_META\type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;

class NewDeviceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $builder
//            ->add('deviceLicense', EntityType::class, [
//                'query_builder' => function(EntityRepository $repository) {
//                    return $repository->createQueryBuilder('device')
//                        ->where('device.locationId is NULL');
//                },
//                'class' => Device::class,
//                'choice_label' => function (Device $nextQuestion) {
//                    return sprintf("Key: " .$nextQuestion->getDeviceLicense() );
//                },
//                'label' => 'Enter device license ',
//                'required' => true,
//                'attr' => ['class' => 'form-control']
//            ])
//            ->add('surveyId', EntityType::class, ['class' => Survey::class,
//                'choice_label' => function(Survey $device) {
//                    return sprintf('Id: '. $device->getSurveyId() .' Title: '.  $device->getSurveyTitle());
//                },
//                'placeholder' => 'Choose an survey',
//                'required' => false,
//                'attr' => ['class' => 'form-control']
//            ])
//            ->add('locationId', EntityType::class, ['class' => Location::class,
//                'choice_label' => function(Location $device) {
//                    return sprintf( $device->getAddress(), $device->getAddress());
//                },
//                'placeholder' => 'Choose an location',
//                'label' => 'Location',
//                'attr' => ['class' => 'form-control']
//            ])
//            /*->add('companies', EntityType::class, [
//                'class' => Company::class,
//                'choice_label' => 'CompanyName',
//
//            ])*/
//
//
//            ->add('save', SubmitType::class, array('label' => 'Create', 'attr' => array('class' => 'btn btn-primary
//                mt-3')))->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Device::class,
        ]);
    }
    public function getBlockPrefix()
    {
        return 'playground_cookiejarbundle_folder';
    }
}