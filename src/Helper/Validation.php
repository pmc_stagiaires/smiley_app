<?php
namespace App\Helper;
use App\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
class Validation {
    public static function request($authHeader, ManagerRegistry $doctrine): ?User{
        $authInformation = 'test';
        $authInformation = explode("|", $authInformation);
        //TODO: make this work with timed accesstokens; future feature
        return $doctrine->getRepository(User::class)->findOneBy(["accessToken" => $authInformation[0]]);
    }
}